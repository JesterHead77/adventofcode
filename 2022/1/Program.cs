﻿int maxCal = 0;
int maxCal2 = 0;
int maxCal3 = 0;

string[] lines = System.IO.File.ReadAllLines("input.txt");

var cal = 0;
foreach (string line in lines)
{
    if (line == ""){
        if (cal > maxCal){
            maxCal3 = maxCal2;
            maxCal2 = maxCal;
            maxCal = cal;
        }else if(cal > maxCal2){
            maxCal3 = maxCal2;
            maxCal2 = cal;
        }else if(cal > maxCal3){
            maxCal3 = cal;
        }
        cal = 0;
        continue;
    }
    cal += int.Parse(line);
}

Console.WriteLine(maxCal+maxCal2+maxCal3);