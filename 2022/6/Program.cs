﻿string stream = System.IO.File.ReadAllText("input.txt");

//-------part 1 -----
Console.WriteLine(DetectDistinct(4));
//-------part 2 -----
Console.WriteLine(DetectDistinct(14));


int DetectDistinct(int howMany)
{
    char[] buffer = new char[howMany];
    for (int i = 0; i < stream.Length; i++)
    {
        Array.Copy(buffer, 1, buffer, 0, buffer.Length - 1); //buffer<<1
        buffer[howMany-1] = stream[i];
        if (buffer.Distinct().Count() == howMany && i > howMany)
        {
            Console.WriteLine(buffer);
            return (i + 1);
        }
    }
    return -1;
}


