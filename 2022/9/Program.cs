﻿string[] lines = System.IO.File.ReadAllLines("input.txt");

Rope Part1 = new Rope(2);
Rope Part2 = new Rope(10);

foreach (var line in lines)
{
    Part1.ProcessInput(line);
    Part2.ProcessInput(line);
}

Console.WriteLine(Part1.TailPositions);
Console.WriteLine(Part2.TailPositions);


class Rope
{
    readonly public int Length;
    public Coor[] Segments;
    private List<Coor> _allTailPositions = new();

    public int TailPositions
    {
        get
        {
            return _allTailPositions.DistinctBy(p => new { p.X, p.Y }).Count();
        }
    }

    public Rope(int len)
    {
        Length = len;
        Segments = new Coor[len];
        _allTailPositions.Add(new Coor(0, 0)); //starting pos
    }

    public void ProcessInput(string input)
    {
        var com = input.Split(" ");

        for (int i = 0; i < Int32.Parse(com[1]); i++)
        {
            switch (com[0])
            {
                case "L":
                    Segments[0].X--;
                    break;
                case "R":
                    Segments[0].X++;
                    break;
                case "U":
                    Segments[0].Y++;
                    break;
                case "D":
                    Segments[0].Y--;
                    break;
            }
            UpdateTail();
        }
    }

    private void UpdateTail()
    {
        for (int i = 1; i < Length; i++)
        {
            Segments[i] = CalculateSegment(Segments[i - 1], Segments[i]);
        }
        _allTailPositions.Add(new Coor(Segments[Length - 1].X, Segments[Length - 1].Y));
    }

    private Coor CalculateSegment(Coor head, Coor tail)
    {
        Coor tmp = new(tail.X, tail.Y);

        if (head.X - tail.X == 2 && head.Y == tail.Y) tmp.X++;
        else if (head.X - tail.X == -2 && head.Y == tail.Y) tmp.X--;
        else if (head.Y - tail.Y == 2 && head.X == tail.X) tmp.Y++;
        else if (head.Y - tail.Y == -2 && head.X == tail.X) tmp.Y--;
        else if ((head.X - tail.X == 1 && head.Y - tail.Y == 2) ||
        (head.X - tail.X == 2 && head.Y - tail.Y == 1) ||
        (head.X - tail.X == 2 && head.Y - tail.Y == 2))
        {
            tmp.Y++;
            tmp.X++;
        }
        else if ((head.X - tail.X == 1 && head.Y - tail.Y == -2) ||
        (head.X - tail.X == 2 && head.Y - tail.Y == -1) ||
        (head.X - tail.X == 2 && head.Y - tail.Y == -2))
        {
            tmp.Y--;
            tmp.X++;
        }
        else if ((head.X - tail.X == -1 && head.Y - tail.Y == 2) ||
        (head.X - tail.X == -2 && head.Y - tail.Y == 1) ||
        (head.X - tail.X == -2 && head.Y - tail.Y == 2))
        {
            tmp.Y++;
            tmp.X--;
        }
        else if ((head.X - tail.X == -1 && head.Y - tail.Y == -2) ||
        (head.X - tail.X == -2 && head.Y - tail.Y == -1) ||
        (head.X - tail.X == -2 && head.Y - tail.Y == -2))
        {
            tmp.Y--;
            tmp.X--;
        }

        return tmp;
    }
}
public struct Coor
{
    public int X;
    public int Y;
    public Coor(int x, int y)
    {
        X = x;
        Y = y;
    }
}