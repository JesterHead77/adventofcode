﻿string[] lines = System.IO.File.ReadAllLines("input.txt");

MyDir root = new("/");
List<MyDir> allDirs = new();

MyDir curDir = root;


foreach (var line in lines)
{
    if (line.StartsWith("$ ls")) continue;
    else if (line.StartsWith("$ cd .."))
    {
        curDir = curDir.ParentDir;
        continue;
    }
    else if (line.StartsWith("$ cd /"))
    {
        curDir = root;
        continue;
    }
    else if (line.StartsWith("$ cd"))
    {
        var tm = curDir.SubDirs.Where(dir => dir.Name == line.Substring(5)).FirstOrDefault();
        if (tm != null) curDir = tm;
        else throw new Exception("Sth went wrong!");
        continue;
    }

    if (line.StartsWith("dir"))
    {
        var dd = new MyDir(line.Substring(4)) { ParentDir = curDir };
        curDir.SubDirs.Add(dd);
        allDirs.Add(dd);
        continue;
    }

    string[] words = line.Split(" ");
    int size = Int32.Parse(words[0]);
    var fname = words[1];

    curDir.Files.Add(new MyFile(size, fname));

}

//------- part 1 ---------

int bigTotal = 0;
foreach (var dir in allDirs.Where(dir => dir.TotalSize <= 100000))
{
    // Console.WriteLine($"Name: {dir.Name}, size: {dir.TotalSize}");
    bigTotal += dir.TotalSize;
}

Console.WriteLine(bigTotal);

//-------- part 2 ---------
int totalDiskSpace = 70000000;
int spaceNeeded = 30000000;
int freeSpace = totalDiskSpace - root.TotalSize;

var bigEnough = allDirs.Where(dir => dir.TotalSize >= spaceNeeded - freeSpace);
var smallest = bigEnough.OrderBy(dir => dir.TotalSize).First();

Console.WriteLine(smallest.TotalSize);

class MyDir
{
    public int TotalSize
    {
        get
        {
            int total = 0;
            foreach (var f in Files) total += f.Size;
            foreach (var dir in SubDirs) total += dir.TotalSize;
            return total;
        }
    }
    public string Name;
    public List<MyDir> SubDirs = new();
    public List<MyFile> Files = new();
    public MyDir ParentDir;
    public MyDir(string name)
    {
        Name = name;
    }
}
class MyFile
{
    public int Size;
    public string Name;
    public MyFile(int size, string name)
    {
        Size = size;
        Name = name;
    }
}