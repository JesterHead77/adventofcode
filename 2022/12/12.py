#Dijkstra'a algorithym
import copy
from typing import List

class Node():
    def __init__(self, posy: int, posx: int):
        self.x = posx
        self.y = posy
        self.elevation: chr
        self.dist = 99999

class Day12():
    def __init__(self) -> None:
        self.nodes:List[List[Node]] = []  #const
        self.nodes_set = set()      
        self.starting_nodes: List[Node] = []
        self.endNode: Node
        self.startNode: Node
        self.node_S: Node

    def part1(self):
        print(
            self.calculate_least_stesp(self.node_S, self.endNode)
        )

    def part2(self):
        distance_list = []
        for nod in self.starting_nodes:
            dd = self.calculate_least_stesp(nod, self.endNode)
            distance_list.append(dd)

        print(min(distance_list))

    def read_input(self, filename: str):

        with open(filename, 'r') as f:
            i: int = 0
            for line in f.readlines():
                j: int = 0
                nodesline: List[Node] = []
                for char in line:
                    node = Node(i,j)
                    node.elevation = char
                    if char == 'S':
                        self.node_S = node
                    if (char in ['S', 'a']):
                        node.elevation = 'a'
                        self.starting_nodes.append(node)
                    if char == 'E' : self.endNode = node
                    self.nodes_set.add(node)
                    nodesline.append(node)
                    j += 1
                self.nodes.insert(i, nodesline) 
                i += 1  

    def calculate_least_stesp(self, starting_node: Node, end_node: Node) -> int:
        elevation_map = copy.deepcopy(self.nodes)
        starting_node = copy.deepcopy(starting_node)
        end_node = copy.deepcopy(end_node)

        nodes_set = set()

        for line in elevation_map:
            for node in line:
                if (node.x == starting_node.x and node.y == starting_node.y):
                    starting_node = node
                if (node.x == end_node.x and node.y == end_node.y):
                    end_node = node
                nodes_set.add(node)

        starting_node.dist=0

        while len(nodes_set) != 0:
            neighbours: List[Node] = []
            
            if starting_node.x > 0:
                neighbours.append(elevation_map[starting_node.y][starting_node.x-1])
            if starting_node.x < len(elevation_map[0])-1:
                neighbours.append(elevation_map[starting_node.y][starting_node.x+1])
            if starting_node.y > 0:
                neighbours.append(elevation_map[starting_node.y-1][starting_node.x])
            if starting_node.y < len(elevation_map)-1:
                neighbours.append(elevation_map[starting_node.y+1][starting_node.x])
            
            for nn in neighbours:
                if nn in nodes_set:
                    alt = starting_node.dist + self.get_distance(starting_node.elevation, nn.elevation)
                    if alt < nn.dist:
                        nn.dist = alt

            nodes_set.remove(starting_node)
            if len(nodes_set) != 0:
                starting_node = min(nodes_set, key=lambda n:n.dist)

        return(end_node.dist)


    def get_distance(self, a:chr, b:chr):
        a = 'a' if a == 'S' else a
        b = 'z' if b == 'E' else b
        return 1 if (ord(b) - ord(a) < 2 ) else 99999

def main():
    day12 = Day12()
    day12.read_input("input.txt")

    day12.part1()
    day12.part2()


if __name__ == "__main__":
    main()