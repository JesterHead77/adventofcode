﻿string[] lines = System.IO.File.ReadAllLines("input.txt");

int cycle = 1;
int sum = 0;
int regx = 1;

foreach (var line in lines)
{
    string[] coms = line.Split(" ");

    switch (coms[0])
    {
        case ("noop"):
            CheckCondition();
            cycle++;
            break;
        case ("addx"):
            CheckCondition();
            cycle ++;
            CheckCondition();
            cycle ++;
            regx += Int32.Parse(coms[1]);
            break;
    }

}

void CheckCondition(){
    if (cycle == 20 || cycle == 60 || cycle == 100 || cycle == 140 || cycle == 180 || cycle == 220){
        sum += regx * cycle;
    }

    if (cycle % 40-1 == regx || cycle % 40-1 == regx-1 || cycle % 40-1 == regx + 1){
        Console.Write("#");
    }else{
        Console.Write(".");
    }


    if (cycle%40==0){
        Console.WriteLine("");
    }

}

Console.WriteLine(sum);

