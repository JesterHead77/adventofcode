﻿using System.Text.RegularExpressions; 
string[] lines = System.IO.File.ReadAllLines("input.txt");

int count = 0;
int count2 = 0;
string rxPatern = "[0-9]+";
Regex rg = new Regex(rxPatern);
foreach (string line in lines)
{
    var matches = rg.Matches(line);

// ------ part 1 ------
    count += 
        (Int32.Parse(matches[0].ToString()) >= Int32.Parse(matches[2].ToString()) && 
        Int32.Parse(matches[1].ToString()) <= Int32.Parse(matches[3].ToString())) || 
        (Int32.Parse(matches[0].ToString()) <= Int32.Parse(matches[2].ToString()) && 
        Int32.Parse(matches[1].ToString()) >= Int32.Parse(matches[3].ToString()))? 1: 0;

// ----- part 2 ------
    count2 += 
        (Int32.Parse(matches[2].ToString()) >= Int32.Parse(matches[0].ToString()) &&
        Int32.Parse(matches[2].ToString()) <= Int32.Parse(matches[1].ToString())) || 
        (Int32.Parse(matches[3].ToString()) >= Int32.Parse(matches[0].ToString()) &&
        Int32.Parse(matches[3].ToString()) <= Int32.Parse(matches[1].ToString())) ||
         (Int32.Parse(matches[0].ToString()) >= Int32.Parse(matches[2].ToString()) && 
        Int32.Parse(matches[1].ToString()) <= Int32.Parse(matches[3].ToString())) || 
        (Int32.Parse(matches[0].ToString()) <= Int32.Parse(matches[2].ToString()) && 
        Int32.Parse(matches[1].ToString()) >= Int32.Parse(matches[3].ToString()))? 1:0;
}

Console.WriteLine(count);
Console.WriteLine(count2);

