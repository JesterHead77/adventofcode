﻿string[] lines = System.IO.File.ReadAllLines("input.txt");

int gridSize = lines[0].Length;

// ------ part 1 -----
int countVisible = 0;
for (int i = 1; i < gridSize - 1; i++)
{
    for (int j = 1; j < gridSize - 1; j++)
    {

        int mid = lines[i][j] - '0';
        bool l = true;
        bool r = true;
        bool u = true;
        bool d = true;

        for (int k = j - 1; k > -1; k--)
        {
            if (mid <= lines[i][k] - '0')
            {
                //not fromm left
                l = false;
                break;

            }
        }
        for (int k = j + 1; k < gridSize; k++)
        {
            if (mid <= lines[i][k] - '0')
            {
                r = false;
                break;
            }
        }
        for (int k = i - 1; k > -1; k--)
        {
            if (mid <= lines[k][j] - '0')
            {
                u = false;
                break;
            }
        }
        for (int k = i + 1; k < gridSize; k++)
        {
            if (mid <= lines[k][j] - '0')
            {
                d = false;
                break;
            }
        }

        countVisible += (r || l || u || d) ? 1 : 0;


    }
}

Console.WriteLine(gridSize * 4 - 4 + countVisible);

// ---- part 2 -----

int biggest = 0;

for (int i = 0; i < gridSize; i++)
{
    for (int j = 0; j < gridSize; j++)
    {
        int mid = lines[i][j] - '0';
        int l = 0;
        int r = 0;
        int u = 0;
        int d = 0;

        for (int k = j - 1; k > -1; k--)
        {
            if (lines[i][k] - '0' < mid)
            {
                l++;
            }
            else
            {
                l++;
                break;
            }
        }
        for (int k = j + 1; k < gridSize; k++)
        {
            if (lines[i][k] - '0' < mid)
            {
                r++;
            }
            else
            {
                r++;
                break;
            }
        }
        for (int k = i - 1; k > -1; k--)
        {
            if (mid > lines[k][j] - '0')
            {
                u++;
            }
            else
            {
                u++;
                break;
            }
        }
        for (int k = i + 1; k < gridSize; k++)
        {
            if (mid > lines[k][j] - '0')
            {
                d++;
            }
            else
            {
                d++;
                break;
            }
        }

        biggest = l*r*u*d > biggest ? l*r*u*d : biggest;
    }
}

Console.WriteLine(biggest);