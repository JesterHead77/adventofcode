﻿// A X rock     1
// B Y paper    2
// C Z scissors 3

int score = 0;

string[] lines = System.IO.File.ReadAllLines("input.txt");

foreach (string line in lines)
{
  switch (line[0]){
    case ('A'):
        switch (line[2]){
            case ('X'): // draw
            score += 3+1;
            break;
            case ('Y'): // win
            score += 6+2;
            break;
            case ('Z'): // loss
            score += 0+3;
            break;
        }
        break;
    case ('B'):
        switch (line[2]){
            case ('X'): // loss
            score += 0+1;
            break;
            case ('Y'): // draw
            score += 3+2;
            break;
            case ('Z'): // win
            score += 6+3;
            break;
        }
        break;
    case ('C'):
        switch (line[2]){
            case ('X'): // win
            score += 6+1;
            break;
            case ('Y'): // loss
            score += 0+2;
            break;
            case ('Z'): // draw
            score += 3+3;
            break;
        }

        break;
  }
}

Console.WriteLine(score);

//part TWO

// X lose
// Y draw
// Z win

score = 0;

foreach (string line in lines)
{
  switch (line[0]){
    case ('A'):
        switch (line[2]){
            case ('X'): // loss
            score += 0+3; //scisors
            break;
            case ('Y'): // draw
            score += 3+1; //rock
            break;
            case ('Z'): // win
            score += 6+2; //paper
            break;
        }
        break;
    case ('B'):
        switch (line[2]){
            case ('X'): // loss
            score += 0+1; //rock
            break;
            case ('Y'): // draw
            score += 3+2; //paper
            break;
            case ('Z'): // win
            score += 6+3; //scissors
            break;
        }
        break;
    case ('C'):
        switch (line[2]){
            case ('X'): // loss
            score += 0+2; //paper
            break;
            case ('Y'): // draw
            score += 3+3; //scisoors
            break;
            case ('Z'): // win
            score += 6+1; //rock
            break;
        }

        break;
  }
}

Console.WriteLine(score);