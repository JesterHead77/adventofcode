class Cave():
    def __init__(self) -> None:
        self.sand = 0
        self.full = False
        self.cave = []
        self.border = 200
        

    def create_cave(self):
        self.cave = [ '.'*600 for j in range(175)]
        rocks = []
        with open("input.txt", 'r') as f:
            for line in f.readlines():
                rocks.append(line.split("->"))

        # create cave
        for rock in rocks:
            for i in range(len(rock)-1):
                xy = rock[i].split(',')
                xy2 = rock[i+1].split(',')

                self.draw_line(int(xy[0]), int(xy[1]), int(xy2[0]), int(xy2[1]))

        # self.cave[0] = self.cave[0][:500-self.border-1] + '+' + self.cave[0][500-self.border:]

    def add_floor(self):
        self.cave[172] = '#' * 600


    def draw_line(self, x1:int,y1:int,x2:int,y2:int):
        if x1 == x2:
            y_min = min(y1,y2)
            y_max = max(y1,y2)
            while y_min <= y_max:
                line = self.cave[y_min]
                new_line = line[:x1-self.border-1] + '#' + line[x1-self.border:]
                self.cave[y_min] = new_line
                y_min += 1
            return
        
        x_min = min(x1,x2)
        x_max = max(x1,x2)

        line = self.cave[y1]
        new_line = line[:x_min-self.border-1] + '#' * (x_max-x_min+1) + line[x_max-self.border:] 
        self.cave[y1] = new_line


    def sand_stoped(self, x,y):
        self.cave[y] = self.cave[y][:x-1] + 'o' + self.cave[y][x:]
        self.sand += 1

    def go_down(self, x,y):
        if y > 174:
            self.full = True
            return
        if self.is_stuff(500-self.border,0):
            self.full = True
            return
        if self.is_stuff(x,y+1):
            if self.is_stuff(x-1,y+1):
                if self.is_stuff(x+1,y+1):
                    self.sand_stoped(x,y)
                    return
                else:
                    self.go_down(x+1,y+1)
                    return
            else:
                self.go_down(x-1,y+1)
                return
        else:
            self.go_down(x,y+1)
            return


    def is_stuff(self, x,y) -> bool:
        if y > 174:
            self.full = True
            return
        if self.cave[y][x-1] == '.':
            return False
        return True

    def add_sand(self):
        while not self.full:
            x,y = 500-self.border, 0
            self.go_down(x,y)

    def save_cave(self):
        with open("cave.txt", 'w') as c:
            for line in self.cave:
                c.write(line +"\n")

    def part1(self):
        self.create_cave()
        self.add_sand()
        print(self.sand)

    def part2(self):
        self.sand = 0
        self.full = False
        self.create_cave()
        self.add_floor()       
        self.add_sand()
        print(self.sand) 


cave = Cave()
cave.part1()
cave.save_cave()
cave.part2()



