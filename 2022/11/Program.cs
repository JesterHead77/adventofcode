﻿using System.Numerics;

string[] lines = System.IO.File.ReadAllLines("input.txt");


List<Monke> Monkes = new();

int m = 0;
foreach (var line in lines)
{
    if (line.Contains("Starting"))
    {
        Monkes.Add(new Monke());
        var words = line.Split(" ");
        foreach (var word in words)
        {
            var w = word.Replace(",", "");
            try
            {
                Monkes[m].Items.Add(Int32.Parse(w));
            }
            catch
            {

            }
        }
        m++;
    }
}

long product = 11*5*19*13*7*17*2*3;

for (int i = 0; i < 10000; i++)
{
    for (int k = 0; k < Monkes.Count(); k++)
    {
        foreach (var item in Monkes[k].Items)
        {
           
            long cur = 0;
            switch (k)
            {
                case 0:
                    cur = item * 5;
                    // cur = cur /3;
                    cur = cur % product;
                    if (cur % 11 == 0)
                    {
                        Monkes[2].Items.Add(cur);
                    }
                    else
                    {
                        Monkes[3].Items.Add(cur);
                    }
                    break;
                case 1:
                    cur = (item * 11);
                    cur = cur % product;
                    // cur = cur /3;
                    if (cur % 5 == 0)
                    {
                        Monkes[4].Items.Add(cur);
                    }
                    else
                    {
                        Monkes[0].Items.Add(cur);
                    }
                    break;
                case 2:
                    cur = item + 2;
                    cur = cur % product;
                    // cur = cur /3;
                    if (cur % 19 == 0)
                    {
                        Monkes[5].Items.Add(cur);
                    }
                    else
                    {
                        Monkes[6].Items.Add(cur);
                    }
                    break;
                case 3:
                    cur = item + 5;
                    cur = cur % product;
                    // cur = cur /3;
                    if (cur % 13 == 0)
                    {
                        Monkes[2].Items.Add(cur);
                    }
                    else
                    {
                        Monkes[6].Items.Add(cur);
                    }
                    break;
                case 4:
                    cur = item * item;
                    cur = cur % product;
                    // cur = cur /3;
                    if (cur % 7 == 0)
                    {
                        Monkes[0].Items.Add(cur);
                    }
                    else
                    {
                        Monkes[3].Items.Add(cur);
                    }
                    break;
                case 5:
                    cur = item + 4;
                    cur = cur % product;
                    // cur = cur /3;
                    if (cur % 17 == 0)
                    {
                        Monkes[7].Items.Add(cur);
                    }
                    else
                    {
                        Monkes[1].Items.Add(cur);
                    }
                    break;
                case 6:
                    cur = item + 6;
                    cur = cur % product;
                    // cur = cur /3;
                    if (cur % 2 == 0)
                    {
                        Monkes[7].Items.Add(cur);
                    }
                    else
                    {
                        Monkes[5].Items.Add(cur);
                    }
                    break;
                case 7:
                    cur = item + 7;
                    cur = cur % product;
                    // cur = cur /3;
                    if (cur % 3 == 0)
                    {
                        Monkes[4].Items.Add(cur);
                    }
                    else
                    {
                        Monkes[1].Items.Add(cur);
                    }
                    break;
            }
            Monkes[k].Inspects++;
        }

        Monkes[k].Items.Clear();

    }
}

long biggest=0;
long biggest2=0;
foreach(var mon in Monkes){
    if (mon.Inspects > biggest){
        biggest2 = biggest;
        biggest = mon.Inspects;
        continue;
    }
    if (mon.Inspects > biggest2){
        biggest2 = mon.Inspects;
    }
}


Console.WriteLine(biggest*biggest2);

class Monke
{
    public List<long> Items = new();
    public List<long> Pending = new();
    public int Inspects = 0;
    public Monke() { }

}