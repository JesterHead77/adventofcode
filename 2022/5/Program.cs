﻿// [T]     [Q]             [S]        
// [R]     [M]             [L] [V] [G]
// [D] [V] [V]             [Q] [N] [C]
// [H] [T] [S] [C]         [V] [D] [Z]
// [Q] [J] [D] [M]     [Z] [C] [M] [F]
// [N] [B] [H] [N] [B] [W] [N] [J] [M]
// [P] [G] [R] [Z] [Z] [C] [Z] [G] [P]
// [B] [W] [N] [P] [D] [V] [G] [L] [T]
//  1   2   3   4   5   6   7   8   9 

using System.Text.RegularExpressions;
string[] lines = System.IO.File.ReadAllLines("input.txt");
 
List<Stack<char>> stacks = new();
char[] s1 = new char[]{'b','p','n','q','h','d','r','t'};
char[] s2 = new char[]{'w','g','b','j','t','v'};
char[] s3 = new char[]{'n','r','h','d','s','v','m','q'};
char[] s4 = new char[]{'p','z','n','m','c'};
char[] s5 = new char[]{'d','z','b'};
char[] s6 = new char[]{'v','c','w','z'};
char[] s7 = new char[]{'g','z','n','c','v','q','l','s'};
char[] s8 = new char[]{'l','g','j','m','d','n','v'};
char[] s9 = new char[]{'t','p','m','f','z','c','g'};
stacks.Add(new Stack<char>(s1));
stacks.Add(new Stack<char>(s2));
stacks.Add(new Stack<char>(s3));
stacks.Add(new Stack<char>(s4));
stacks.Add(new Stack<char>(s5));
stacks.Add(new Stack<char>(s6));
stacks.Add(new Stack<char>(s7));
stacks.Add(new Stack<char>(s8));
stacks.Add(new Stack<char>(s9));


string rxPatern = "[0-9]+";
Regex rg = new Regex(rxPatern);
foreach (string line in lines)
{
    var matches = rg.Matches(line);
    int cnt = Int32.Parse(matches[0].ToString());
    int from = Int32.Parse(matches[1].ToString());
    int to = Int32.Parse(matches[2].ToString());

    // // ------ part 1 ------
    // for (int i = 0; i<cnt; i++){
    //     stacks[to-1].Push(stacks[from-1].Pop());
    // }

    // ------ part 2 ------
    Stack<char> tmpStack = new();
    for (int i = 0; i<cnt; i++){
        tmpStack.Push(stacks[from-1].Pop());
    }
    for (int i = 0; i<cnt; i++){
        stacks[to-1].Push(tmpStack.Pop());
    }

}

foreach (var st in stacks){
    Console.Write(st.First().ToString().ToUpper());
}
Console.WriteLine();
