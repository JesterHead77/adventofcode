﻿string[] lines = System.IO.File.ReadAllLines("input.txt");

// a -96 = 1
// A -38 = 27

// ------ part 1 ------
int sum = 0;
foreach (string line in lines)
{
    string h1 = line.Substring(0, line.Length / 2 );
    string h2 = line.Substring(line.Length / 2);
    var common = h1.Intersect(h2).FirstOrDefault();
    sum += char.IsUpper(common) ? common - 38 : common - 96;
}

Console.WriteLine(sum);

// ----- part 2 ------

sum = 0;

for (int i = 0; i < lines.Length; i += 3){
    var common = lines[i].Intersect(lines[i+1]).Intersect(lines[i+2]).FirstOrDefault();
    sum += char.IsUpper(common) ? common - 38 : common - 96;
}

Console.WriteLine(sum);
