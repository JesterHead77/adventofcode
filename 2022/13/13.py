def comparePair(p1: str, p2: str):
    for i in range(len(p1)):
        if p1[i] == "[": 
            if p2[i] == "]":
                # print("Not in right order, rigt roun out of items")
                return False
            if p2[i] != "[":
                        
                item1 = ""
                j = 0
                tmp = p2[i]
                while tmp not in [",","]"]:
                    item1 += tmp
                    j += 1
                    tmp = p2[i+j]

                p2 = p2[:i] + f"[{item1}]" + p2[i+j:]
            continue
        if p2[i] == "[": 
            if p1[i] == "]":
                # print(" right order, left run out of items")
                return True
            if p1[i] != "[":
                item1 = ""
                j = 0
                tmp = p1[i]
                while tmp not in [",","]"]:
                    item1 += tmp
                    j += 1
                    tmp = p1[i+j]

                p1 = p1[:i] + f"[{item1}]" + p1[i+j:]
            continue
        if p1[i] == "]":
            if p2[i] != "]":
                # print("right order, left run out of items")
                return True
            #end list
            continue
        if p2[i] == "]":
            if p1[i] != "]":
                # print("wrong order, right run out of items")
                return False
            #end list
            continue
        if p1[i] == ",":
            # next item
            continue
        
        item1 = ""
        j = 0
        tmp = p1[i]
        while tmp not in [",","]"]:
            item1 += tmp
            j += 1
            tmp = p1[i+j]
        p1 = p1[:i+1] + p1[i+j:]

        item2 = ""
        j = 0
        tmp = p2[i]
        while tmp not in [",","]"]:
            item2 += tmp
            j += 1
            tmp = p2[i+j]
        p2 = p2[:i+1] + p2[i+j:]


        if int(item1) > int(item2):
            # print("Not in right order")
            return False
        if int(item1) < int(item2):
            # print("In right order")
            return True

    # print("String are the same so right order")
    return True


def bubbleSort(alist):
    for passnum in range(len(alist)-1,0,-1):
        for i in range(passnum):
            if not comparePair(alist[i], alist[i+1]):
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp

def part1():
    sum = 0
    with open("input.txt", 'r') as f:
        lines = f.readlines()
        for i in range(0, len(lines), 3):
            if comparePair(lines[i],lines[i+1]):
                sum += (i+3) / 3
    print(sum)

def part2():
    with open("input.txt", 'r') as f:
        lines = f.readlines()
        del lines[3-1::3]
    lines.append("[[2]]")
    lines.append("[[6]]")
            
    bubbleSort(lines)
    key = 0
    i =1 
    for line in lines:
        if (line) == "[[2]]":
            key = i
        if (line) == "[[6]]":
            key = key*i
        i += 1
    print(key)

            


import unittest

class TestSum(unittest.TestCase):
    def test_1(self):
        s1 = "[1,1,3,1,1]"
        s2 = "[1,1,5,1,1]"
        self.assertEqual(comparePair(s1,s2),True)

    def test_2(self):
        s1 = "[[1],[2,3,4]]"
        s2 = "[[1],4]"
        self.assertEqual(comparePair(s1,s2),True)

    def test_3(self):
        s1 = "[9]"
        s2 = "[[8,7,6]]"
        self.assertEqual(comparePair(s1,s2),False)

    def test_4(self):
        s1 = "[[4,4],4,4]"
        s2 = "[[4,4],4,4,4]"
        self.assertEqual(comparePair(s1,s2),True)

    def test_5(self):
        s1 = "[7,7,7,7]"
        s2 = "[7,7,7]"
        self.assertEqual(comparePair(s1,s2),False)

    def test_6(self):
        s1 = "[]"
        s2 = "[1,1,3,1,1]"
        self.assertEqual(comparePair(s1,s2),True)

    def test_7(self):
        s1 = "[[]]"
        s2 = "[]"
        self.assertEqual(comparePair(s1,s2),False)

    def test_8(self):
        s1 = "[1,[2,[3,[4,[5,6,7]]]],8,9]"
        s2 = "[1,[2,[3,[4,[5,6,0]]]],8,9]"
        self.assertEqual(comparePair(s1,s2),False)

    def test_9(self):
        s1 = "[10]"
        s2 = "[9]"
        self.assertEqual(comparePair(s1,s2),False)



if __name__ == '__main__':
    # unittest.main()
    part1()
    part2()
