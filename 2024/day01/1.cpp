#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int main() {
    std::ifstream file("input.txt");
    std::vector<int> left;
    std::vector<int> right;

    std::string line;
    while (std::getline(file, line)) {
        std::stringstream ss(line);
        int num1, num2;
        ss >> num1 >> num2;

        left.push_back(num1);
        right.push_back(num2);
    }

    //part 1
    std::sort(left.begin(), left.end());
    std::sort(right.begin(), right.end());

    long sum = 0;

    for (int i=0; i<left.size(); i++){
        sum += abs(left[i] - right[i]);
    }
    std::cout << sum << std::endl;

    //part 2
    long sum2 = 0;
    for (auto leftt : left){
        int times = 0;
        for (auto rightt : right){
            if (leftt == rightt){
                times++;
            }
        }
        sum2 += static_cast<long>(leftt * times);
    }
    std::cout << sum2 << std::endl;

    return 0;
}
