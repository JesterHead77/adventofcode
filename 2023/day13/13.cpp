#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

int sumMirrorLines(std::vector<std::vector<std::vector<char>>> &patterns, int smudges);
int findMirrorLine(std::vector<std::vector<char>> &pattern, int smudges);
int findMirrorColumn(std::vector<std::vector<char>> &pattern, int smudges);

int main() {
    std::fstream file("input.txt");

    std::vector<std::vector<std::vector<char>>> patterns;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    std::vector<std::vector<char>> pattern;
    do {
        std::getline(file, line);
        if (line.empty()) {
            patterns.push_back(pattern);
            pattern.clear();
            continue;
        }
        std::vector<char> temp_line;
        for (int j = 0; j < line.length(); j++) {
            temp_line.push_back(line[j]);
        }
        pattern.push_back(temp_line);
    } while (!file.eof());

    std::cout << "Part1: " << sumMirrorLines(patterns, 0) << "\n";
    std::cout << "Part2: " << sumMirrorLines(patterns, 1) << "\n";

    return 0;
}

int sumMirrorLines(std::vector<std::vector<std::vector<char>>> &patterns, int smudges) {
    int sum = 0;

    int a = 0;
    int index_line = 0;
    for (auto patern : patterns) {

        index_line = findMirrorLine(patern, smudges);
        if (index_line > 0) {
            sum += index_line * 100;
            continue;
        }

        index_line = findMirrorColumn(patern, smudges);

        if (index_line > 0) {
            sum += index_line;
            continue;
        }

        std::cout << "Ptaren has no mirror line: " << a << std::endl;
        a++;
    }

    return sum;
}

int findMirrorLine(std::vector<std::vector<char>> &pattern, int smudges) {
    bool mirror = false;
    for (int i = 1; i < pattern.size(); i++) {
        int smudgesCount = 0;

        for (int k = 0; k < pattern[0].size(); k++) { // check row
            if (pattern[i - 1][k] == pattern[i][k]) {
                mirror = true;
                continue;
            }

            smudgesCount++;
            if (smudgesCount <= smudges) {
                continue;
            }

            mirror = false;
            break;
        }

        if (!mirror) { // if first row does not match check nexr row
            continue;
        }

        for (int j = 1; j < std::min(i, (int)pattern.size() - i); j++) { // check if all other rows are mirrored

            for (int k = 0; k < pattern[0].size(); k++) {
                if (pattern[i - 1 - j][k] == pattern[i + j][k]) {
                    mirror = true;
                    continue;
                }

                smudgesCount++;
                if (smudgesCount <= smudges) {
                    continue;
                }

                mirror = false;
                break;
            }

            if (!mirror) { // if next line dont match find new first line
                break;
            }
        }

        if (!mirror) { // if first line match check nexr lines
            continue;
        }

        if (smudgesCount != smudges) {
            continue;
        }

        return i;
    }
    return -1;
}

int findMirrorColumn(std::vector<std::vector<char>> &pattern, int smudges) {
    bool mirror = false;

    for (int i = 1; i < pattern[0].size(); i++) {
        int smudgesCount = 0;
        for (int k = 0; k < pattern.size(); k++) { // check column
            if (pattern[k][i - 1] == pattern[k][i]) {
                mirror = true;
                continue;
            }

            smudgesCount++;
            if (smudgesCount <= smudges) {
                continue;
            }

            mirror = false;
            break;
        }

        if (!mirror) { // if first column match check nexr columns
            continue;
        }

        for (int j = 1; j < std::min(i, (int)pattern[0].size() - i); j++) { // check if all other columns  are mirrored

            for (int k = 0; k < pattern.size(); k++) {
                if (pattern[k][i - 1 - j] == pattern[k][i + j]) {
                    mirror = true;
                    continue;
                }

                smudgesCount++;
                if (smudgesCount <= smudges) {
                    continue;
                }

                mirror = false;
                break;
            }

            if (!mirror) { // if next column dont match find new first column
                break;
            }
        }

        if (!mirror) { // if first column match check nexr lines
            continue;
        }

        if (smudgesCount != smudges) {
            continue;
        }
        return i;
    }

    return -1;
}