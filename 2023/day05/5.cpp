#include "split.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

unsigned int part1(std::vector<std::vector<std::vector<unsigned int>>> &maps, std::vector<unsigned int> &seeds);
unsigned int part2(std::vector<std::vector<std::vector<unsigned int>>> &maps, std::vector<unsigned int> &seeds);
unsigned int get_destination(std::vector<std::vector<unsigned int>> &map, unsigned int source);

int main() {
    std::fstream file("input.txt");

    std::vector<unsigned int> seeds;
    std::vector<std::vector<std::vector<unsigned int>>> maps;

    const std::string topic[7] = {
        "seed-to-soil",
        "soil-to-fertilizer",
        "fertilizer-to-water",
        "water-to-light",
        "light-to-temperature",
        "temperature-to-humidity",
        "humidity-to-location"};

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;
    int k = 0;

    while (std::getline(file, line)) {

        std::vector<std::string> words = my::split(line, ' ');
        if (words.empty()) {
            continue;
        }

        if (words[0] == "seeds:") {
            for (int i = 1; i < words.size(); i++) {
                seeds.push_back(std::stoul(words[i]));
            }
            continue;
        }

        if (words[0] == topic[k]) {
            maps.push_back(std::vector<std::vector<unsigned int>>());
            k++;
            continue;
        }

        std::vector<unsigned int> temp;
        for (int i = 0; i < words.size(); i++) {
            temp.push_back(std::stoul(words[i]));
        }

        maps[k - 1].push_back(temp);
    }

    std::cout << "Part1: " << part1(maps, seeds) << "\n";
    std::cout << "Part2: " << part2(maps, seeds) << "\n";

    return 0;
}

unsigned int part1(std::vector<std::vector<std::vector<unsigned int>>> &maps, std::vector<unsigned int> &seeds) {
    unsigned int smallest = 0xFFFFFFFF;

    for (int s = 0; s < seeds.size(); s++) {
        int loc = seeds[s];
        for (int i = 0; i < 7; i++) {
            loc = get_destination(maps[i], loc);
        }
        if (loc < smallest) {
            smallest = loc;
        }
    }

    return smallest;
}

unsigned int get_destination(std::vector<std::vector<unsigned int>> &map, unsigned int source) {
    for (int i = 0; i < map.size(); i++) {
        if (map[i][1] <= source && map[i][1] + map[i][2] > source) { // range contains source
            return map[i][0] + source - map[i][1];
        }
    }
    return source; // no range contains source
}

unsigned int part2(std::vector<std::vector<std::vector<unsigned int>>> &maps, std::vector<unsigned int> &seeds) {
    unsigned int smallest = 0xFFFFFFFF;

    for (int s = 0; s < seeds.size(); s += 2) {
        for (int ss = seeds[s]; ss < seeds[s + 1] + seeds[s]; ss++) { //brute force
            int loc = ss;
            for (int i = 0; i < 7; i++) {
                loc = get_destination(maps[i], loc);
            }
            if (loc < smallest) {
                smallest = loc;
            }
        }
    }

    return smallest;
}