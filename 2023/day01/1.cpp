#include <fstream>
#include <iostream>
#include <string>
#include <vector>


uint64_t sum_calibration_part1(std::vector<std::string> &data);
uint64_t sum_calibration_part2(std::vector<std::string> &data);
char find_string_number(std::string &str);

int main() {
    std::vector<std::string> data;
    std::fstream file("input.txt");
    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;
    while (std::getline(file, line)) {
        data.push_back(line);
    }


    std::cout << "Calibration sum is: " << sum_calibration_part1(data) << "\n";
    std::cout << "Calibration sum is: " << sum_calibration_part2(data) << "\n";

    return 0;
}

uint64_t sum_calibration_part1(std::vector<std::string> &data) {
    uint64_t sum = 0;
    for (int i = 0; i < data.size(); i++) {
        char number[2] = "";
        int j = 0;
        while (true) {
            if (isdigit(data[i][j])) {
                number[0] = data[i][j];
                break;
            }
            j++;
        }
        j = data[i].length();
        while (true) {
            if (isdigit(data[i][j])) {
                number[1] = data[i][j];
                break;
            }
            j--;
        }
        sum += atoi(number);
    }
    return sum;
}

char find_string_number(std::string &str) {

    if (str.find("one") == 0) {
        return '1';
    } else if (str.find("two") == 0) {
        return '2';
    } else if (str.find("three") == 0) {
        return '3';
    } else if (str.find("four") == 0) {
        return '4';
    } else if (str.find("five") == 0) {
        return '5';
    } else if (str.find("six") == 0) {
        return '6';
    } else if (str.find("seven") == 0) {
        return '7';
    } else if (str.find("eight") == 0) {
        return '8';
    } else if (str.find("nine") == 0) {
        return '9';
    }

    return '0';
}

uint64_t sum_calibration_part2(std::vector<std::string> &data) {
    uint64_t sum = 0;

    for (int i = 0; i < data.size(); i++) {
        char number[2] = "";
        int j = 0;
        while (true) {

            std::basic_string sub = data[i].substr(j, 5);
            char result = find_string_number(sub);

            if (result != '0') {
                number[0] = result;
                break;
            } else if (isdigit(data[i][j])) {
                number[0] = data[i][j];
                break;
            }

            j++;
        }
        j = data[i].length();
        while (true) {

            std::basic_string sub = data[i].substr(j, 5);
            char result = find_string_number(sub);

            if (result != '0') {
                number[1] = result;
                break;
            } else if (isdigit(data[i][j])) {
                number[1] = data[i][j];
                break;
            }
            j--;
        }
        sum += atoi(number);
    }
    return sum;
}
