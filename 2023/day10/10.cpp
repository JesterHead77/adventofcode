#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

// .F-7.
// .|.|.
// .L-J.

enum Direction {
    North = 1,
    East = 2,
    West = 3,
    South = 4,
    None
};

std::unordered_map<Direction, std::vector<char>> pipes = {
    {Direction::North, {'F', '|', '7', 'S'}},
    {Direction::South, {'L', '|', 'J', 'S'}},
    {Direction::West, {'F', '-', 'L', 'S'}},
    {Direction::East, {'7', '-', 'J', 'S'}},
};

int part1(const char (&map)[140][140], int s_line, int s_col);
int part2();
bool match(const char pipe1, Direction Dir);

char loop[140][140];

int main() {
    std::fstream file("input.txt");
    char map[140][140];
    int s_line = 0;
    int s_col = 0;

    for (int i = 0; i < 140; ++i) {
        for (int j = 0; j < 140; ++j) {
            loop[i][j] = '.'; // Initialize each element with a dot
        }
    }

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    int i = 0;
    std::string line;
    while (std::getline(file, line)) {
        for (int j = 0; j < line.size(); j++) {
            map[i][j] = line[j];
            if (line[j] == 'S') {
                s_line = i;
                s_col = j;
            }
        }
        i++;
    }

    std::cout << "Part1: " << part1(map, s_line, s_col) << "\n";
    std::cout << "Part2: " << part2() << "\n";

    return 0;
}

int part1(const char (&map)[140][140], int s_line, int s_col) {
    int total_length = 0;
    int l = s_line;
    int c = s_col;
    Direction past_dir = None;

    while (true) {
        if (map[l][c] == 'S' && total_length > 0) {
            // loop[l][c] = 'D';            //this is hard coded
            total_length++;
            return total_length / 2;
        }
        if (map[l][c] == '|' || map[l][c] == 'L' || map[l][c] == 'J' || map[l][c] == 'S') {
            if (match(map[l - 1][c], Direction::North) && past_dir != Direction::North && l > 0) { // north
                if (map[l][c] == '|') {  //this is for part 2
                    loop[l][c] = 'U';
                } else {
                    loop[l][c] = '-';
                }
                l--;
                past_dir = Direction::South;
                total_length++;
                continue;
            }
        }
        if (map[l][c] == '|' || map[l][c] == 'F' || map[l][c] == '7' || map[l][c] == 'S') {
            if (match(map[l + 1][c], Direction::South) && past_dir != Direction::South && l < sizeof(map)) { // south
                loop[l][c] = 'D';
                l++;
                past_dir = Direction::North;
                total_length++;
                continue;
            }
        }
        if (map[l][c] == '-' || map[l][c] == '7' || map[l][c] == 'J' || map[l][c] == 'S') {
            if (match(map[l][c - 1], Direction::West) && past_dir != Direction::West && c > 0) { // west

                if (map[l][c] == '7') {
                    loop[l][c] = 'U';
                } else {
                    loop[l][c] = '-';
                }

                c--;
                past_dir = Direction::East;
                total_length++;
                continue;
            }
        }
        if (map[l][c] == '-' || map[l][c] == 'F' || map[l][c] == 'L' || map[l][c] == 'S') {
            if (match(map[l][c + 1], Direction::East) && past_dir != Direction::East && c < sizeof(map[0])) { // east
                if (map[l][c] == 'F') {
                    loop[l][c] = 'U';
                } else {
                    loop[l][c] = '-';
                }
                c++;
                past_dir = Direction::West;
                total_length++;
                continue;
            }
        }
    }
}

bool match(const char pipe, Direction Dir) {

    std::vector<char> matches = pipes[Dir];

    if (std::find(matches.begin(), matches.end(), pipe) != matches.end()) {
        return true;
    }

    return false;
}

int part2() {
    int tiles = 0;

    std::cout << std::endl;
    for (int i = 0; i < 140; i++) {
        int enclosed = 0;
        for (int j = 0; j < 140; j++) {
            std::cout << loop[i][j];
            if (loop[i][j] == 'U') {
                enclosed++;
            }
            if (loop[i][j] == 'D') {
                enclosed--;
            }
            if (enclosed != 0 && loop[i][j] == '.') {
                tiles++;
            }
        }
        std::cout << std::endl;
    }

    return tiles;
}