#include <algorithm>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <unordered_map>
#include <vector>

struct CubeCoor {
    int x, y, z;
};

struct Brick {
    CubeCoor start, end;
    char orient; // x / y / z
    int len;
    std::vector<Brick *> suportedby;
    std::vector<Brick *> suports;

    void addSuportedby(Brick *newBrick) {
        for (auto brick : suportedby) {
            if (brick == newBrick) {
                return;
            }
        }
        this->suportedby.push_back(newBrick);
    }
    void addSupports(Brick *newBrick) {
        for (auto brick : suports) {
            if (brick == newBrick) {
                return;
            }
        }
        this->suports.push_back(newBrick);
    }
};

struct Coor {
    int x, y;
    bool operator==(const Coor &other) const {
        return (x == other.x && y == other.y);
    }
};

template <>
struct std::hash<Coor> {
    std::size_t operator()(const Coor &c) const {
        std::size_t hashValue = std::hash<int>()(c.x);
        std::size_t hashValue2 = std::hash<int>()(c.y);
        return (hashValue ^ hashValue2) << 1;
    }
};

struct Height {
    int z;
    Brick *brick;
};

std::vector<Brick> bricks;
std::unordered_map<Coor, Height> pile;

void fall();
int part1();
int part2();
// long part2(std::vector<std::string> &garden, Step &startingStep);

void createBrick(std::vector<int> bounds) {
    Brick newBrick;
    // start is closer to origin
    newBrick.start.x = std::min(bounds[0], bounds[3]);
    newBrick.start.y = std::min(bounds[1], bounds[4]);
    newBrick.start.z = std::min(bounds[2], bounds[5]);
    newBrick.end.x = std::max(bounds[0], bounds[3]);
    newBrick.end.y = std::max(bounds[1], bounds[4]);
    newBrick.end.z = std::max(bounds[2], bounds[5]);

    if (newBrick.start.x != newBrick.end.x) {
        newBrick.orient = 'x';
        newBrick.len = newBrick.end.x - newBrick.start.x + 1;
    } else if (newBrick.start.y != newBrick.end.y) {
        newBrick.orient = 'y';
        newBrick.len = newBrick.end.y - newBrick.start.y + 1;
    } else if (newBrick.start.z != newBrick.end.z) {
        newBrick.orient = 'z';
        newBrick.len = newBrick.end.z - newBrick.start.z + 1;
    } else {
        newBrick.orient = 'z';
        newBrick.len = 1;
    }

    bricks.push_back(newBrick);
}

int main() {
    std::fstream file("input.txt");

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    std::regex coorPattern("[0-9]+");
    while (std::getline(file, line)) {

        std::smatch match;
        std::vector<int> matches;
        while (std::regex_search(line, match, coorPattern)) {
            matches.push_back(std::stoi(match[0].str()));
            line = match.suffix();
        }

        createBrick(matches);
    }
    fall();
    std::cout << "Part1: " << part1() << "\n";
    std::cout << "Part2: " << part2() << "\n";

    return 0;
}

bool compare(Brick brick1, Brick brick2) {
    return (brick1.start.z < brick2.start.z);
}

void fall() {
    auto sorter = [](const Brick brick1, const Brick brick2) {
        return compare(brick1, brick2);
    };
    std::sort(bricks.begin(), bricks.end(), sorter);

    for (auto &brick : bricks) {
        std::vector<Coor> coors;

        switch (brick.orient) {
        case 'z': {

            Coor p = {brick.start.x, brick.start.y};
            auto it = pile.find(p);

            if (it == pile.end()) {
                pile[p] = {brick.len, &brick};
            } else {
                brick.addSuportedby(pile[p].brick);
                pile[p].brick->addSupports(&brick);
                pile[p] = {brick.len + pile[p].z, &brick};
            }
            break;
        }
        case 'y': {
            coors.clear();
            int maxZOccupied = 0;

            // find all coors for brick
            for (int i = 0; i < brick.len; i++) {
                Coor p = {brick.start.x, brick.start.y + i};
                auto it = pile.find(p);

                if (it == pile.end()) {
                    pile[p] = {0, nullptr};
                } else if (pile[p].z > maxZOccupied) {
                    // find highest brick alredy on pile
                    maxZOccupied = pile[p].z;
                }
                coors.push_back(p);
            }

            for (auto &coor : coors) {
                if (pile[coor].z == maxZOccupied && maxZOccupied != 0) {
                    brick.addSuportedby(pile[coor].brick);
                    pile[coor].brick->addSupports(&brick);
                }
                pile[coor].z = maxZOccupied + 1;
                pile[coor].brick = &brick;
            }
            break;
        }
        case 'x': {

            coors.clear();
            int maxZOccupied = 0;

            // find all coors for brick
            for (int i = 0; i < brick.len; i++) {
                Coor p = {brick.start.x + i, brick.start.y};
                auto it = pile.find(p);

                if (it == pile.end()) {
                    pile[p] = {0, nullptr};
                } else if (pile[p].z > maxZOccupied) {
                    // find highest brick alredy on pile
                    maxZOccupied = pile[p].z;
                }
                coors.push_back(p);
            }

            for (auto &coor : coors) {
                if (pile[coor].z == maxZOccupied && maxZOccupied != 0) {
                    brick.addSuportedby(pile[coor].brick);
                    pile[coor].brick->addSupports(&brick);
                }
                pile[coor].z = maxZOccupied + 1;
                pile[coor].brick = &brick;
            }
            break;
        }
        default:
            break;
        }
    }
}

int part1() {

    int sum = 0;
    for (auto &brick : bricks) {
        bool canBeDis = true;
        for (auto supportsBrick : brick.suports) {
            if (supportsBrick->suportedby.size() == 1) {
                canBeDis = false;
                break;
            }
        }
        if (canBeDis) {
            sum++;
        }
    }

    return sum;
}

void desintegrateBrick(Brick *brick, std::vector<Brick *> &bricksFelled) {

    std::vector<Brick *> bricksToDis;

    for (auto supportsBrick : brick->suports) {

        int noSupportedBy = supportsBrick->suportedby.size();
        for (auto bb : supportsBrick->suportedby) {
            if (noSupportedBy == 0) {
                break;
            }
            for (auto cc : bricksFelled) {
                if (bb == cc) {
                    noSupportedBy--;
                    break;
                }
            }
        }

        if (noSupportedBy == 0) {
            bricksToDis.push_back(supportsBrick);

            auto it = std::find(bricksFelled.begin(), bricksFelled.end(), supportsBrick);

            if (it != bricksFelled.end()) {
                continue;
            }

            bricksFelled.push_back(supportsBrick);
        }
    }

    for (auto supportsBrick : bricksToDis) {
        desintegrateBrick(supportsBrick, bricksFelled);
    }

    return;
}

int part2() {

    int sum = 0;
    for (auto &brick : bricks) {
        std::vector<Brick *> bricksFelled;
        bricksFelled.push_back(&brick);
        desintegrateBrick(&brick, bricksFelled);
        sum += bricksFelled.size() - 1;
    }

    return sum;
}