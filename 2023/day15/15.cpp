#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int part1(std::vector<std::string> &sequence);
int part2(std::vector<std::string> &platform);
int hash(std::string &str);

struct Lens {
    std::string label;
    int focalLength;
};

int main() {
    std::fstream file("input.txt");

    std::vector<std::string> sequence;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    while (std::getline(file, line)) {

        std::istringstream iss(line);

        std::string word;
        while (std::getline(iss, word, ',')) {
            sequence.push_back(word);
        }
    }

    std::cout << "Part1: " << part1(sequence) << "\n";
    // std::string aaa = "qp";
    // std::cout << "hash: " << hash(aaa) << "\n";
    std::cout << "Part2: " << part2(sequence) << "\n";

    return 0;
}

int part1(std::vector<std::string> &sequence) {
    int sum = 0;

    for (auto &str : sequence) {
        sum += hash(str);
    }

    return sum;
}

int hash(std::string &str) {
    int currentValue = 0;

    for (auto ch : str) {
        currentValue += static_cast<int>(ch);
        currentValue *= 17;
        currentValue %= 256;
    }

    return currentValue;
}

int part2(std::vector<std::string> &platform) {

    std::unordered_map<int, std::vector<Lens>> boxes;

    for (auto &str : platform) {
        int index = 0;
        std::string label;
        bool add = false;
        size_t foundIndex = str.find('=');
        if (foundIndex != std::string::npos) {
            index = static_cast<int>(foundIndex);
            add = true;
        } else {
            index = static_cast<int>(str.find('-'));
        }
        label = str.substr(0, index);
        int box = hash(label);
        std::vector<Lens> lensesInBox = boxes[box];

        if (add) {
            Lens newLens{label, str[index + 1] - '0'};

            if (lensesInBox.size() == 0) {
                lensesInBox.push_back(newLens);
            } else {
                bool alreadyIn = false;
                for (auto &lens : lensesInBox) {
                    if (lens.label == label) {
                        lens = newLens;
                        alreadyIn = true;
                        break;
                    }
                }
                if (!alreadyIn) {
                    lensesInBox.push_back(newLens);
                }
            }
        } else {
            for (int i = 0; i < lensesInBox.size(); i++) {
                if (lensesInBox[i].label == label) {
                    lensesInBox.erase(lensesInBox.begin() + i);
                }
            }
        }

        boxes[box] = lensesInBox;
    }

    int focusingPower = 0;

    for (int b = 0; b < boxes.size(); b++) {
        for (int i = 0; i < boxes[b].size(); i++) {
            focusingPower += (b + 1) * (i + 1) * boxes[b][i].focalLength;
        }
    }

    return focusingPower;
}