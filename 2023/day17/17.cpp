#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <optional>
#include <set>
#include <sstream>
#include <string>
#include <vector>

int part1(std::vector<std::string> &city);
// int part2(std::vector<std::string> &city);

int main() {
    std::fstream file("input.txt");

    std::vector<std::string> city;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    while (std::getline(file, line)) {
        city.push_back(line);
    }

    std::cout << "Part1: " << part1(city) << "\n";
    // std::cout << "Part2: " << part2(city) << "\n";

    return 0;
}

struct Node {
    int id;
    int row;
    int col;
    int dir; // 1 -> rigthward, 2 -> downward, 3 -> leftward, 4 -> upward
    int streak;
    int distToStartNode;
    int parent;
};

int part1(std::vector<std::string> &city) {
    std::set<int> unvisitedNodes;
    std::unordered_map<int, Node> allNodes;

    int limit = 3;
    int id = 0;
    for (int i = 0; i < city.size(); ++i) {
        for (int j = 0; j < city[0].length(); j++) {
            for (int d = 1; d < 5; d++) {
                for (int s = 1; s <= limit; s++) {

                    id++;
                    Node node{id, i, j, d, s, 99999};
                    allNodes[id] = node;
                    unvisitedNodes.insert(id);
                }
            }
        }
    }

    allNodes[1].distToStartNode = 0;
    Node currentNode = allNodes[1];
    while (!unvisitedNodes.empty()) {
        unvisitedNodes.erase(currentNode.id);
        std::vector<int> possibleNextNodes;
        std::vector<int> neighboor;

        if (currentNode.col > 0) { // left
            int dd = 12 * ((currentNode.col - 1) % city.size()) +
                     12 * ((currentNode.row) * city[0].length());
            for (int i = dd; i < dd + 12; i++) {
                neighboor.push_back(i + 1);
            }
        }
        if (currentNode.row > 0) { // up
            int dd = 12 * ((currentNode.col) % city.size()) +
                     12 * ((currentNode.row - 1) * city[0].length());
            for (int i = dd; i < dd + 12; i++) {
                neighboor.push_back(i + 1);
            }
        }
        if (currentNode.col < city[0].length() - 1) { // right
            int dd = 12 * ((currentNode.col + 1) % city.size()) +
                     12 * ((currentNode.row) * city[0].length());
            for (int i = dd; i < dd + 12; i++) {
                neighboor.push_back(i + 1);
            }
        }
        if (currentNode.row < city.size() - 1) { // down
            int dd = 12 * ((currentNode.col) % city.size()) +
                     12 * ((currentNode.row + 1) * city[0].length());
            for (int i = dd; i < dd + 12; i++) {
                neighboor.push_back(i + 1);
            }
        }

        for (const auto &nodeId : neighboor) {
            if (allNodes[nodeId].col == currentNode.col + 1 && allNodes[nodeId].row == currentNode.row &&
                allNodes[nodeId].dir == 1 &&
                currentNode.dir != 3 &&
                ((currentNode.streak < limit && currentNode.dir == 1 && allNodes[nodeId].streak > currentNode.streak) || (currentNode.dir != 1))) {
                possibleNextNodes.push_back(nodeId);
                continue;
            }
            if (allNodes[nodeId].col == currentNode.col - 1 && allNodes[nodeId].row == currentNode.row &&
                allNodes[nodeId].dir == 3 &&
                currentNode.dir != 1 &&
                ((currentNode.streak < limit && currentNode.dir == 3 && allNodes[nodeId].streak > currentNode.streak) || (currentNode.dir != 3))) {
                possibleNextNodes.push_back(nodeId);
                continue;
            }
            if (allNodes[nodeId].col == currentNode.col && allNodes[nodeId].row == currentNode.row + 1 &&
                allNodes[nodeId].dir == 2 &&
                currentNode.dir != 4 &&
                ((currentNode.streak < limit && currentNode.dir == 2 && allNodes[nodeId].streak > currentNode.streak) || (currentNode.dir != 2))) {
                possibleNextNodes.push_back(nodeId);
                continue;
            }
            if (allNodes[nodeId].col == currentNode.col && allNodes[nodeId].row == currentNode.row - 1 &&
                allNodes[nodeId].dir == 4 &&
                currentNode.dir != 2 &&
                ((currentNode.streak < limit && currentNode.dir == 4 && allNodes[nodeId].streak > currentNode.streak) || (currentNode.dir != 4))) {
                possibleNextNodes.push_back(nodeId);
                continue;
            }
        }

        for (auto key : possibleNextNodes) {
            auto it = unvisitedNodes.find(key);

            if (it != unvisitedNodes.end()) {
                int dist = currentNode.distToStartNode + city[allNodes[key].row][allNodes[key].col] - '0';

                if (dist < allNodes[key].distToStartNode) {
                    allNodes[key].distToStartNode = dist;
                    allNodes[key].parent = currentNode.id;
                }
            }
        }

        int smallest = 999999;
        int smallestId = -1;
        for (auto nodeId : unvisitedNodes) {
            if (allNodes[nodeId].distToStartNode < smallest) {
                smallest = allNodes[nodeId].distToStartNode;
                smallestId = allNodes[nodeId].id;
            }
        }

        currentNode = allNodes[smallestId];
    }

    int smallest = 9999999;
    int smallestId = -1;
    for (auto node : allNodes) {
        if (node.second.row == city.size() - 1 && node.second.col == city[0].length() - 1) {
            if (node.second.distToStartNode < smallest) {
                smallest = node.second.distToStartNode;
                smallestId = node.second.id;
            }
        }
    }

    std::vector<int> path;

    int cc = smallestId;
    while (true) {
        path.push_back(allNodes[cc].id);
        cc = allNodes[cc].parent;
        if (cc < 2) {
            break;
        }
    }

    std::cout << std::endl;
    std::cout << std::endl;
    int idd = 0;
    for (int i = 0; i < city.size(); i++) {
        for (int j = 0; j < city[0].length(); j++) {
            idd++;
            std::vector<int>::iterator it;
            bool found = false;

            for (const auto &node : allNodes) {
                if (node.second.row == i && node.second.col == j) {

                    it = std::find(path.begin(), path.end(), node.second.id);
                    if (it != path.end()) {
                        found = true;
                        break;
                    }
                }
            }

            if (found) {
                std::cout << '#';

            } else {
                std::cout << '.';
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    return allNodes[smallestId].distToStartNode;
}
