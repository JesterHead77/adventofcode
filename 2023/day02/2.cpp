#include "split.h"
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

int part1(std::vector<std::string> &data);
int part2(std::vector<std::string> &data);

int main() {
    std::vector<std::string> data;
    std::fstream file("input.txt");
    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;
    while (std::getline(file, line)) {
        data.push_back(line);
    }

    std::cout << "Possible games ids sum is: " << part1(data) << "\n";
    std::cout << "Power of cubes is : " << part2(data) << "\n";

    return 0;
}

int part1(std::vector<std::string> &data) {
    const int RED = 12;
    const int GREEN = 13;
    const int BLUE = 14;

    int sum = 0;

    for (int i = 0; i < data.size(); i++) {
        std::string substring = data[i].substr(data[i].find(":") + 2);
        std::vector<std::string> words = my::split(substring, ' ');
        bool possible = true;
        for (int j = 0; j < words.size(); j += 2) {
            // std::cout << words[j+1] << " ";
            if (words[j + 1].find("red") == 0) {
                if (std::stoi(words[j]) > RED) {
                    possible = false;
                    break;
                }
            }
            if (words[j + 1].find("green") == 0) {
                if (std::stoi(words[j]) > GREEN) {
                    possible = false;
                    break;
                }
            }
            if (words[j + 1].find("blue") == 0) {
                if (std::stoi(words[j]) > BLUE) {
                    possible = false;
                    break;
                }
            }
        }
        if (!possible){continue;}
        sum += i+1;
    }

    return sum;
}

int part2(std::vector<std::string> &data) {

    int sum = 0;

    for (int i = 0; i < data.size(); i++) {
        int red = 0;
        int green = 0;
        int blue = 0;

        std::string substring = data[i].substr(data[i].find(":") + 2);
        std::vector<std::string> words = my::split(substring, ' ');

        for (int j = 0; j < words.size(); j += 2) {
            // std::cout << words[j+1] << " ";
            if (words[j + 1].find("red") == 0) {
                if (std::stoi(words[j]) > red) {
                    red = std::stoi(words[j]);
                }
            }
            if (words[j + 1].find("green") == 0) {
              if (std::stoi(words[j]) > green) {
                    green = std::stoi(words[j]);
                }
            }
            if (words[j + 1].find("blue") == 0) {
              if (std::stoi(words[j]) > blue) {
                    blue = std::stoi(words[j]);
                }
            }
        }

        sum += red * green * blue;
    }

    return sum;
}