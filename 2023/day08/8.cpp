#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <unordered_map>
#include <vector>

int part1(std::unordered_map<std::string, std::vector<std::string>> &network, std::vector<char> &instructions);
unsigned long part2(std::unordered_map<std::string, std::vector<std::string>> &network, std::vector<char> &instructions);

int main() {
    std::fstream file("input.txt");

    std::vector<char> instructions;
    std::unordered_map<std::string, std::vector<std::string>> network;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;
    int i = -1;
    while (std::getline(file, line)) {
        i++;
        if (i == 0) {
            for (auto ch : line) {
                instructions.push_back(ch);
            }
            continue;
        }
        if (line.empty()) {
            continue;
        }

        network[line.substr(0, 3)] = std::vector<std::string>({line.substr(7, 3), line.substr(12, 3)});
    }

    std::cout << "Part1: " << part1(network, instructions) << "\n";
    std::cout << "Part2: " << part2(network, instructions) << "\n";

    return 0;
}

int part1(std::unordered_map<std::string, std::vector<std::string>> &network, std::vector<char> &instructions) {
    int steps = 0;
    std::string key = "AAA";

    int i = 0;
    while (true) {
        if (i == instructions.size()) {
            i = 0;
        }

        if (instructions[i] == 'L') {
            key = network[key][0];
        } else if (instructions[i] == 'R') {
            key = network[key][1];
        }
        steps++;

        if (key == "ZZZ") {
            break;
        }

        i++;
    }
    return steps;
}

unsigned long part2(std::unordered_map<std::string, std::vector<std::string>> &network, std::vector<char> &instructions) {
    int steps = 0;

    std::vector<std::string> keys;
    for (auto &pair : network) {
        if (pair.first[2] == 'A') {
            keys.push_back(pair.first);
        }
    }

    std::vector<std::vector<std::string>> cycles;

    for (auto &key : keys) {
        std::vector<std::string> cycle;
        std::string tempKey = key;
        int i = 0;
        while (true) {
            cycle.push_back(key);

            if (i == instructions.size()) {
                i = 0;
            }

            if (instructions[i] == 'L') {
                key = network[key][0];
            } else if (instructions[i] == 'R') {
                key = network[key][1];
            }
            i++;

            if (key[2] == 'Z') {
                break;
            }
        }
        cycles.push_back(cycle);
    }

    std::vector<long> cycleLengths;

    for (auto cycle : cycles) {
        cycleLengths.push_back(cycle.size());
    }

    return std::accumulate(cycleLengths.begin(), cycleLengths.end(), 1L, std::lcm<long, long>);
}
