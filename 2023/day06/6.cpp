#include <iostream>
#include <vector>
#include <cmath>
#include <iomanip>

int part1();
int part2();

int main() {

    std::cout << "Part1: " << part1() << "\n";
    std::cout << "Part2: " << part2() << "\n";

    return 0;
}

int part1() {
    int times[4] = {35, 69, 68, 87};
    int dists[4] = {213, 1168, 1086, 1248};

    int sum = 1;

    for (int i =0; i<4; i++){
        int ways = 0;
        for (int s=0;s<times[i];s++){
            int dist = (times[i] - s) * s; 
            if (dist > dists[i]){
                ways++;
            }
        }
        sum *= ways;
    }
    return sum;
}

int part2() {
    long double time = 35696887;
    long double dist = 213116810861248;

    long double root1 = time - std::sqrt(std::pow(time,2)-4*dist)/2;
    long double root2 = time + std::sqrt(std::pow(time,2)-4*dist)/2;

    long double ways = std::floor(root2) - std::ceil(root1);

    return (int)ways;
}
