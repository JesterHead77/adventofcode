#include <fstream>
#include <iostream>
#include <set>
#include <sstream>
#include <string>
#include <vector>

int part1();
int part2();

std::vector<std::string> GRAPH;

int main() {
    std::fstream file("input.txt");

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    while (std::getline(file, line)) {
        GRAPH.push_back(line);
    }

    std::cout << "Part1: " << part1() << "\n";
    std::cout << "Part2: " << part2() << "\n";

    return 0;
}

struct Node {
    int row;
    int col;
    int maxPath = 0;
    bool operator==(const Node &other) const {
        return (row == other.row && col == other.col);
    }
    bool operator<(const Node &other) const {
        if (row != other.row) {
            return row < other.row;
        }
        return col < other.col;
    }
};

std::vector<Node> findNeighbours(Node &node) {
    std::vector<Node> neighbours;

    if (node.row > 0 &&
        GRAPH[node.row - 1][node.col] != '#' &&
        (GRAPH[node.row][node.col] == '.' || GRAPH[node.row][node.col] == '^')) {
        neighbours.push_back({node.row - 1, node.col});
    }
    if (node.row < GRAPH.size() - 1 &&
        GRAPH[node.row + 1][node.col] != '#' &&
        (GRAPH[node.row][node.col] == '.' || GRAPH[node.row][node.col] == 'v')) {
        neighbours.push_back({node.row + 1, node.col});
    }
    if (node.col > 0 &&
        GRAPH[node.row][node.col - 1] != '#' &&
        (GRAPH[node.row][node.col] == '.' || GRAPH[node.row][node.col] == '<')) {
        neighbours.push_back({node.row, node.col - 1});
    }
    if (node.col < GRAPH[0].length() - 1 &&
        GRAPH[node.row][node.col + 1] != '#' &&
        (GRAPH[node.row][node.col] == '.' || GRAPH[node.row][node.col] == '>')) {
        neighbours.push_back({node.row, node.col + 1});
    }

    return neighbours;
}

std::vector<Node> findNeighbours2(Node &node) {
    std::vector<Node> neighbours;

    if (node.row > 0 &&
        GRAPH[node.row - 1][node.col] != '#') {
        neighbours.push_back({node.row - 1, node.col});
    }
    if (node.row < GRAPH.size() - 1 &&
        GRAPH[node.row + 1][node.col] != '#') {
        neighbours.push_back({node.row + 1, node.col});
    }
    if (node.col > 0 &&
        GRAPH[node.row][node.col - 1] != '#') {
        neighbours.push_back({node.row, node.col - 1});
    }
    if (node.col < GRAPH[0].length() - 1 &&
        GRAPH[node.row][node.col + 1] != '#') {
        neighbours.push_back({node.row, node.col + 1});
    }

    return neighbours;
}

void dfs(Node &current, int &maxPath, std::set<Node> &visited) {
    visited.insert(current);

    for (auto &nei : findNeighbours(current)) {
        if (visited.find(nei) == visited.end()) {
            nei.maxPath = std::max(nei.maxPath, current.maxPath + 1);
            dfs(nei, maxPath, visited);
        }
    }

    if (current.maxPath > maxPath) {
        maxPath = current.maxPath;
    }
}

void dfs2(Node &current, int &maxPath, std::set<Node> &visited) {
    visited.insert(current);

    for (auto &nei : findNeighbours2(current)) {
        if (visited.find(nei) == visited.end()) {
            nei.maxPath = std::max(nei.maxPath, current.maxPath + 1);
            dfs2(nei, maxPath, visited);
        }
    }

    if (current.row == GRAPH.size() - 1 && current.col == GRAPH[0].length() - 2 && current.maxPath > maxPath) {
        maxPath = current.maxPath;
    }
}

int part1() {
    Node firstNode = {0, 1};
    int max = 0;
    std::set<Node> visited;

    dfs(firstNode, max, visited);

    return max;
}

int part2() {
    Node firstNode = {0, 1};
    int max = 0;
    std::set<Node> visited;

    dfs2(firstNode, max, visited);

    return max;
}
