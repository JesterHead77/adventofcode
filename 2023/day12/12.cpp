#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int calculate_arrangements(std::string spring);
std::vector<int> extractNumbers(const std::string &input);
bool checkSprings(std::string &line, std::vector<int> &nums);

void tryAllCombinations(
    const std::string &str,
    int &arrs,
    std::vector<int> &springs_number,
    int index = 0,
    std::string combination = "");

int main() {
    std::fstream file("input.txt");

    int part1Sum = 0;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;
    while (std::getline(file, line)) {

        part1Sum += calculate_arrangements(line);
    }

    std::cout << "Part1: " << part1Sum << "\n";
    // std::cout << "Part2: " << part2() << "\n";

    return 0;
}

int calculate_arrangements(std::string springs) {
    int arrangemnts = 0;

    std::istringstream iss(springs);
    std::vector<std::string> words;

    std::string word;
    while (iss >> word) {
        words.push_back(word);
    }
    std::vector<int> springs_number = extractNumbers(words[1]);

    tryAllCombinations(words[0], arrangemnts, springs_number);

    return arrangemnts;
}

void tryAllCombinations(
    const std::string &str,
    int &arrs,
    std::vector<int> &springs_number,
    int index,
    std::string combination) {

    if (index == str.length()) {
        if (checkSprings(combination, springs_number)) {
            arrs++;
        }
        return;
    }

    if (str[index] == '?') {
        tryAllCombinations(str, arrs, springs_number, index + 1, combination + '.');
        tryAllCombinations(str, arrs, springs_number, index + 1, combination + '#');
    } else {
        tryAllCombinations(str, arrs, springs_number, index + 1, combination + str[index]);
    }
}

std::vector<int> extractNumbers(const std::string &input) {
    std::vector<int> numbers;
    std::stringstream ss(input);
    std::string token;

    while (std::getline(ss, token, ',')) {
        int number = std::stoi(token);
        numbers.push_back(number);
    }

    return numbers;
}

bool checkSprings(std::string &line, std::vector<int> &nums) {
    std::vector<std::string> springs;
    std::stringstream ss(line);
    std::string token;

    while (std::getline(ss, token, '.')) {
        if (token != "") {
            springs.push_back(token);
        }
    }

    if (nums.size() != springs.size()) {
        return false;
    }

    for (int i = 0; i < nums.size(); i++) {
        if (nums[i] != springs[i].length()) {
            return false;
        }
    }
    return true;
}