#include <fstream>
#include <iostream>
#include <string>
#include <vector>

struct Galaxy {
    int x;
    int y;

    Galaxy(int x, int y) : x(x), y(y){};
};

unsigned long part1(
    std::vector<std::vector<char>> &universe,
    std::vector<Galaxy> &galaxies,
    std::vector<int> &emptyLines,
    std::vector<int> &emptyColumns,
    int expansionRate);
void expandUniverse(std::vector<std::vector<char>> &universe, std::vector<int> &emptyLines, std::vector<int> &emptyColumns);
void findGalaxies(std::vector<std::vector<char>> &universe, std::vector<Galaxy> &galaxies);

int main() {
    std::fstream file("input.txt");

    std::vector<std::vector<char>> universe;
    std::vector<Galaxy> galaxies;
    std::vector<int> emptyLines;
    std::vector<int> emptyColumns;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;

    int i = 0;
    while (std::getline(file, line)) {
        std::vector<char> temp_line;
        for (int j = 0; j < line.length(); j++) {
            temp_line.push_back(line[j]);
        }
        universe.push_back(temp_line);
    }

    expandUniverse(universe, emptyLines, emptyColumns);
    findGalaxies(universe, galaxies);

    std::cout << "Part1: " << part1(universe, galaxies, emptyLines, emptyColumns, 1) << "\n";
    std::cout << "Part2: " << part1(universe, galaxies, emptyLines, emptyColumns, 1000000 - 1) << "\n";

    return 0;
}

unsigned long part1(std::vector<std::vector<char>> &universe, std::vector<Galaxy> &galaxies, std::vector<int> &emptyLines, std::vector<int> &emptyColumns, int expansionRate) {
    unsigned long sum = 0;

    for (int k = 0; k < universe.size(); k++) {
        for (int j = 0; j < universe[0].size(); j++) {
            if (universe[k][j] != '#') {
                continue;
            }
            for (auto &galaxy : galaxies) {
                if (galaxy.x == k && galaxy.y == j) {
                    continue;
                }
                int lines = 0;
                int columns = 0;

                for (auto l : emptyLines) {
                    if ((l > k && l < galaxy.x) || (l < k && l > galaxy.x)) {
                        lines++;
                    }
                }
                for (auto c : emptyColumns) {
                    if ((c > j && c < galaxy.y) || (c < j && c > galaxy.y)) {
                        columns++;
                    }
                }

                sum += abs(galaxy.x - k) + abs(galaxy.y - j) + (lines + columns) * expansionRate;
            }
        }
    }

    return sum / 2;
}

void expandUniverse(std::vector<std::vector<char>> &universe, std::vector<int> &emptyLines, std::vector<int> &emptyColumns) {

    // check lines
    for (int i = 0; i < universe.size(); i++) {
        bool toExpand = true;
        for (int j = 0; j < universe[0].size(); j++) {
            if (universe[i][j] == '#') {
                toExpand = false;
            }
        }
        if (toExpand) {
            emptyLines.push_back(i);
        }
    }

    // check columns
    for (int i = 0; i < universe[0].size(); i++) {
        bool toExpand = true;
        for (int j = 0; j < universe.size(); j++) {
            if (universe[j][i] == '#') {
                toExpand = false;
            }
        }
        if (toExpand) {
            emptyColumns.push_back(i);
        }
    }
}

void findGalaxies(std::vector<std::vector<char>> &universe, std::vector<Galaxy> &galaxies) {
    for (int k = 0; k < universe.size(); k++) {
        for (int j = 0; j < universe[0].size(); j++) {
            if (universe[k][j] == '#') {
                galaxies.push_back(Galaxy(k, j));
            }
        }
    }
}
