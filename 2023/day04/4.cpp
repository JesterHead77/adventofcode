#include "split.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

int part1(std::vector<std::vector<int>> &wins, std::vector<std::vector<int>> &my);
int part2(std::vector<std::vector<int>> &wins, std::vector<std::vector<int>> &my);

int main() {
    std::fstream file("input.txt");
    std::vector<std::vector<int>> winning_numbers;
    std::vector<std::vector<int>> scratch_numbers;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;
    while (std::getline(file, line)) {

        std::string substring = line.substr(line.find(":") + 2);
        std::vector<std::string> words = my::split(substring, ' ');

        std::vector<int> win;
        std::vector<int> my;
        for (int i = 0; i < words.size(); i++) {
            if (i < 10) {
                win.push_back(std::stoi(words[i]));
            } else if (i > 10) {
                my.push_back(std::stoi(words[i]));
            }
        }

        winning_numbers.push_back(win);
        scratch_numbers.push_back(my);
    }

    std::cout << "Scratchcards poit sum is: " << part1(winning_numbers, scratch_numbers) << "\n";
    std::cout << "Scratchcards sum is: " << part2(winning_numbers, scratch_numbers) << "\n";

    return 0;
}

int part1(std::vector<std::vector<int>> &wins, std::vector<std::vector<int>> &my) {
    int sum = 0;

    for (int i = 0; i < wins.size(); i++) {
        int won = 0;
        for (int j = 0; j < my[i].size(); j++) {
            for (int k = 0; k < wins[i].size(); k++) {
                if (my[i][j] == wins[i][k]) {
                    won++;
                }
            }
        }
        if (won == 1) {
            sum++;
        } else if (won > 1) {
            sum += pow(2, won - 1);
        }
    }

    return sum;
}

int part2(std::vector<std::vector<int>> &wins, std::vector<std::vector<int>> &my) {
    int sum = 0;

    std::vector<int> cards(wins.size(), 1);

    for (int i = 0; i < wins.size(); i++) {
        int won = 0;
        for (int j = 0; j < my[i].size(); j++) {
            for (int k = 0; k < wins[i].size(); k++) {
                if (my[i][j] == wins[i][k]) {
                    won++;
                }
            }
        }

        for (int m = i + 1; m < won + i + 1; m++) {
            cards[m] += 1 * cards[i];
        }
    }

    for (int i = 0; i < cards.size(); i++) {
        sum += cards[i];
    }

    return sum;
}