#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

struct Part {
    int x = 0;
    int m = 0;
    int a = 0;
    int s = 0;
};

struct Range {
    int min = 1;
    int max = 4000;
};
struct XmasRange {
    Range x;
    Range m;
    Range a;
    Range s;
};

int part1();
unsigned long long part2();
bool accepted(Part const &part, std::string startingWorkflow = "in");
void traverseGraph(std::vector<XmasRange> &allValidRanges, std::string startingWorkflow = "in", std::vector<std::string> path = {}, std::vector<bool> truRange = {});
void evalPath(std::vector<XmasRange> &allValidRanges, std::vector<std::string> path);

std::vector<Part> parts;
std::unordered_map<std::string, std::vector<std::string>> workflows;
int main() {
    std::fstream file("input.txt");

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    bool wf = true;
    std::regex workflowsPattern("[xmas][<>][0-9]*:[a-zA-Z]*");
    std::regex lastPattern("[a-zA-Z]*}");
    std::regex partPattern("[xmas]=[0-9]*");
    while (std::getline(file, line)) {

        if (line == "") {
            wf = false;
            continue;
        }
        if (wf) {
            size_t pos = line.find('{');
            std::string name = line.substr(0, pos);

            std::vector<std::string> workflow;
            std::smatch match;
            while (std::regex_search(line, match, workflowsPattern)) {

                workflow.push_back(match[0].str());
                line = match.suffix();
            }
            match = std::smatch();
            if (std::regex_search(line, match, lastPattern)) {
                for (const auto &match : match) {
                    workflow.push_back(match.str().substr(0, match.str().length() - 1));
                }
            }
            workflows.insert(std::make_pair(name, workflow));
        } else {
            Part part;
            std::smatch match;

            while (std::regex_search(line, match, partPattern)) {

                switch (match[0].str()[0]) {
                case 'x':
                    part.x = std::stoi(match[0].str().substr(2));
                    break;
                case 'm':
                    part.m = std::stoi(match[0].str().substr(2));
                    break;
                case 'a':
                    part.a = std::stoi(match[0].str().substr(2));
                    break;
                case 's':
                    part.s = std::stoi(match[0].str().substr(2));
                    break;
                default:
                    break;
                }
                line = match.suffix();
            }
            parts.push_back(part);
        }
    }

    std::cout << "Part1: " << part1() << "\n";
    std::cout << "Part2: " << part2() << "\n";

    return 0;
}

int part1() {
    int sum = 0;

    for (auto const &part : parts) {
        if (accepted(part)) {
            sum += part.x + part.m + part.a + part.s;
        }
    }

    return sum;
}

unsigned long long part2() {

    std::vector<XmasRange> allValidRanges;

    traverseGraph(allValidRanges);

    unsigned long long sum = 0;
    for (auto const &range : allValidRanges) {
        sum += (static_cast<long long>(range.x.max - range.x.min + 1) *
                static_cast<long long>(range.m.max - range.m.min + 1) *
                static_cast<long long>(range.a.max - range.a.min + 1) *
                static_cast<long long>(range.s.max - range.s.min + 1));
    }

    return sum;
}

void evalPath(std::vector<XmasRange> &allValidRanges, std::vector<std::string> path, std::vector<bool> truRange) {
    XmasRange xmasRange;

    for (int i = 0; i < path.size(); i++) {

        int num = std::stoi(path[i].substr(2));

        switch (path[i][0]) {
        case 'x':
            xmasRange.x.max = (path[i][1] == '<' && truRange[i]) ? num -1 : xmasRange.x.max;
            xmasRange.x.min = (path[i][1] == '>' && truRange[i]) ? num +1 : xmasRange.x.min;
            xmasRange.x.max = (path[i][1] == '>' && !truRange[i]) ? num : xmasRange.x.max;
            xmasRange.x.min = (path[i][1] == '<' && !truRange[i]) ? num : xmasRange.x.min;
            break;
        case 'm':
            xmasRange.m.max = (path[i][1] == '<' && truRange[i]) ? num -1 : xmasRange.m.max;
            xmasRange.m.min = (path[i][1] == '>' && truRange[i]) ? num +1 : xmasRange.m.min;
            xmasRange.m.max = (path[i][1] == '>' && !truRange[i]) ? num : xmasRange.m.max;
            xmasRange.m.min = (path[i][1] == '<' && !truRange[i]) ? num : xmasRange.m.min;
            break;
        case 'a':
            xmasRange.a.max = (path[i][1] == '<' && truRange[i]) ? num -1 : xmasRange.a.max;
            xmasRange.a.min = (path[i][1] == '>' && truRange[i]) ? num +1 : xmasRange.a.min;
            xmasRange.a.max = (path[i][1] == '>' && !truRange[i]) ? num : xmasRange.a.max;
            xmasRange.a.min = (path[i][1] == '<' && !truRange[i]) ? num : xmasRange.a.min;
            break;
        case 's':
            xmasRange.s.max = (path[i][1] == '<' && truRange[i]) ? num -1 : xmasRange.s.max;
            xmasRange.s.min = (path[i][1] == '>' && truRange[i]) ? num +1 : xmasRange.s.min;
            xmasRange.s.max = (path[i][1] == '>' && !truRange[i]) ? num : xmasRange.s.max;
            xmasRange.s.min = (path[i][1] == '<' && !truRange[i]) ? num : xmasRange.s.min;
            break;
        default:
            break;
        }
    }
    allValidRanges.push_back(xmasRange);
}

void traverseGraph(std::vector<XmasRange> &allValidRanges, std::string startingWorkflow, std::vector<std::string> path, std::vector<bool> truRange) {

    if (startingWorkflow == "A") {
        evalPath(allValidRanges, path, truRange);
        return;
    }
    for (int i = 0; i < workflows[startingWorkflow].size(); i++) {

        if (i > 0) {
            truRange.pop_back();
            truRange.push_back(false);
        }
        if (workflows[startingWorkflow][i][1] == '<' || workflows[startingWorkflow][i][1] == '>') {

            path.push_back(workflows[startingWorkflow][i]);
            truRange.push_back(true);

            auto nextWorkflow = workflows[startingWorkflow][i].substr(workflows[startingWorkflow][i].find(':') + 1);
            traverseGraph(allValidRanges, nextWorkflow, path, truRange);
            continue;
        } else {
            traverseGraph(allValidRanges, workflows[startingWorkflow][i], path, truRange);
            return;
        }
    }
    return;
}

bool accepted(const Part &part, std::string startingWorkflow) {

    std::string workflow = startingWorkflow;
    for (auto const &instruction : workflows[workflow]) {
        if (instruction[1] == '<' || instruction[1] == '>') {
            int num = std::stoi(instruction.substr(2));
            bool result;
            switch (instruction[0]) {
            case 'x':
                result = (instruction[1] == '<') ? (part.x < num) : (part.x > num);
                break;
            case 'm':
                result = (instruction[1] == '<') ? (part.m < num) : (part.m > num);
                break;
            case 'a':
                result = (instruction[1] == '<') ? (part.a < num) : (part.a > num);
                break;
            case 's':
                result = (instruction[1] == '<') ? (part.s < num) : (part.s > num);
                break;
            default:
                break;
            }
            if (!result) {
                continue;
            }
            workflow = instruction.substr(instruction.find(':') + 1);
            break;
        }
        workflow = instruction;
        break;
    }

    if (workflow != "A" && workflow != "R") {
        return accepted(part, workflow);
    }
    if (workflow == "A") {
        return true;
    }
    return false;
}
