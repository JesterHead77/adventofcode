#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

int part1(std::vector<std::string> &data);
unsigned long part2(std::vector<std::string> &data);
int find_num(std::vector<std::string> &data, int i, int j);

int main() {
    std::vector<std::string> data;
    std::fstream file("input.txt");
    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;
    while (std::getline(file, line)) {
        data.push_back(line);
    }

    std::cout << "Parts number sum is: " << part1(data) << "\n";
    std::cout << "Sum of gear ratio is : " << part2(data) << "\n";

    return 0;
}

int part1(std::vector<std::string> &data) {

    int sum = 0;

    for (int i = 0; i < data.size(); i++) {
        for (int j = 0; j < data[i].length(); j++) {
            if (isdigit(data[i][j])) {

                bool partnumber = false;
                char num_buffer[5] = "";
                num_buffer[0] = data[i][j];
                int k = 1;
                int last = j;
                while (true) {
                    if ((j + k) <= data[i].length()) {
                        if (isdigit(data[i][j + k])) {
                            num_buffer[k] = data[i][j + k];
                            last++;
                            k++;
                            continue;
                        }
                        break;
                    }
                }
                // row to the left
                if (j > 0) {
                    if (data[i][j - 1] != '.') {
                            partnumber = true;
                        }
                    if (i > 0) {
                        if (data[i - 1][j - 1] != '.') {
                            partnumber = true;
                        }
                    }
                    if (i < data.size() - 1) {
                        if (data[i + 1][j - 1] != '.') {
                            partnumber = true;
                        }
                    }
                }
                // row to the right
                if (last < data[i].length()) {
                    if (data[i][last + 1] != '.') {
                        partnumber = true;
                    }
                    if (i > 0) {
                        if (data[i - 1][last + 1] != '.') {
                            partnumber = true;
                        }
                    }
                    if (i < data.size() - 1) {
                        if (data[i + 1][last + 1] != '.') {
                            partnumber = true;
                        }
                    }
                }
                // row above
                if (i > 0) {
                    for (int l = j; l <= last; l++) {
                        if (data[i - 1][l] != '.') {
                            partnumber = true;
                        }
                    }
                }
                // row below
                if (i < data.size() - 1) {
                    for (int l = j; l <= last; l++) {
                        if (data[i + 1][l] != '.') {
                            partnumber = true;
                        }
                    }
                }

                j = last; // skip number chars
                if (partnumber) {
                    sum += atoi(num_buffer);
                }
            }
        }
    }
    return sum;
}

int find_num(std::vector<std::string> &data, int i, int j) {
    char buffer[5] = "";

    // find first num
    int k = 1;
    while (true) {
        if (j - k >= 0) {
            if (!isdigit(data[i][j - k])) {
                break;
            }
            k++;
        }
        if (j-k<0){break;}
    }

    int l = 1;
    while (true) {
        if (!isdigit(data[i][j - k + l])) {
            break;
        }
        buffer[l-1] = data[i][j - k + l];
        l++;
    }

    return atoi(buffer);
}

unsigned long part2(std::vector<std::string> &data) {

    double sum = 0;

    for (int i = 0; i < data.size(); i++) {
        for (int j = 0; j < data[i].length(); j++) {
            if (data[i][j] == '*') {
                std::vector<int> adjesent_nums;

                int num1 = -1;
                int num2 = -1;
                int num3 = -1;


                //top row
                if (i>0 && j-1>=0){
                    if (isdigit(data[i-1][j-1])){
                        num1 = find_num(data, i-1, j-1);
                    }
                }
                if (i>0 && j+1<=data[i].length()){
                    if (isdigit(data[i-1][j+1])){
                        num2 = find_num(data, i-1, j+1);
                    }
                }
                if (i>0 ){
                    if (isdigit(data[i-1][j])){
                        num3 = find_num(data, i-1, j);
                    }
                }

                if (num1 >-1 && num1 == num2){
                    adjesent_nums.push_back(num1);
                }
                else if (num1 > -1 && num2 > -1 && num1 != num2){
                    adjesent_nums.push_back(num1);
                    adjesent_nums.push_back(num2);
                }else if (num1 > -1){
                    adjesent_nums.push_back(num1);
                }else if (num2 > -1){
                    adjesent_nums.push_back(num2);
                }else if (num3>-1 && num1 == -1 && num2 == -1){
                    adjesent_nums.push_back(num3);
                }

                //botom row
                num1 = -1;
                num2 = -1;
                num3 = -1;
                if (i<data.size()-1 && j-1>=0){
                    if (isdigit(data[i+1][j-1])){
                        num1 = find_num(data, i+1, j-1);
                    }
                }
                if (i<data.size()-1  && j+1<=data[i].length()){
                    if (isdigit(data[i+1][j+1])){
                        num2 = find_num(data, i+1, j+1);
                    }
                }
                if (i<data.size()-1 ){
                    if (isdigit(data[i+1][j])){
                        num2 = find_num(data, i+1, j);
                    }
                }

                if (num1 >-1 && num1 == num2){
                    adjesent_nums.push_back(num1);
                }
                else if (num1 > -1 && num2 > -1 && num1 != num2){
                    adjesent_nums.push_back(num1);
                    adjesent_nums.push_back(num2);
                }else if (num1 > -1){
                    adjesent_nums.push_back(num1);
                }else if (num2 > -1){
                    adjesent_nums.push_back(num2);
                }else if (num3>-1 && num1 == -1 && num2 == -1){
                    adjesent_nums.push_back(num3);
                }

                //left
                if (j-1>0){
                    if (isdigit(data[i][j-1])){
                        adjesent_nums.push_back(find_num(data,i,j-1));
                    }
                }
                //right
                if (j+1<=data[i].length()){
                    if (isdigit(data[i][j+1])){
                        adjesent_nums.push_back(find_num(data,i,j+1));
                    }
                }


                if (adjesent_nums.size() == 2){
                    sum += adjesent_nums[0] * adjesent_nums[1];
                }


            }
        }
    }
    return sum;
}
// 66759900