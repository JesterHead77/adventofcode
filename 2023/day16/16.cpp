#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

struct Beam {
    int row = 0;
    int col = 0;
    int dir = 1; // 1 -> rigthward, 2 -> downward, 3 -> leftward, 4 -> upward
};

int part1(std::vector<std::string> &floor, Beam beam);
int part2(std::vector<std::string> &floor);
void traceBeam(std::vector<std::string> &floor, std::vector<std::string> &lava, Beam beam);

struct Lens {
    std::string label;
    int focalLength;
};

int main() {
    std::fstream file("input.txt");

    std::vector<std::string> floor;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    while (std::getline(file, line)) {
        floor.push_back(line);
    }

    std::cout << "Part1: " << part1(floor, {0, -1, 1}) << "\n";
    std::cout << "Part2: " << part2(floor) << "\n";

    return 0;
}

int part1(std::vector<std::string> &floor, Beam beam) {
    std::vector<std::string> lava(floor.size(), std::string(floor[0].size(), '.'));
    traceBeam(floor, lava, beam);
    int energized = 0;

    // std::cout << "\n";
    for (auto row : lava) {
        // std::cout << row << "\n";
        for (auto tile : row) {
            if (tile == '#') {
                energized++;
            }
        }
    }

    return energized;
}

int part2(std::vector<std::string> &floor) {
    int maxEnergy = 0;

    for (int i = 0; i < floor.size(); i++) {
        int en = part1(floor, {i, -1, 1});
        if (en > maxEnergy) {
            maxEnergy = en;
        }
    }
    for (int i = 0; i < floor.size(); i++) {
        int en = part1(floor, {i, floor[0].length(), 3});
        if (en > maxEnergy) {
            maxEnergy = en;
        }
    }
    for (int i = 0; i < floor[0].length(); i++) {
        int en = part1(floor, {-1, i, 2});
        if (en > maxEnergy) {
            maxEnergy = en;
        }
    }
    for (int i = 0; i < floor[0].length(); i++) {
        int en = part1(floor, {floor.size(), i, 4});
        if (en > maxEnergy) {
            maxEnergy = en;
        }
    }
    return maxEnergy;
}

void traceBeam(std::vector<std::string> &floor, std::vector<std::string> &lava, Beam beam) {
    if (beam.col > -1 && beam.row > -1 && beam.row < floor.size() && beam.col < floor[0].length()) {
        lava[beam.row][beam.col] = '#';
    }
    int loopLentgh = 100;
    int lavaCount = 1;
    char prevTile = '.';
    int nextRow;
    int nextCol;

    while (true) {

        switch (beam.dir) {
        case 1:
            nextRow = beam.row;
            nextCol = beam.col + 1;
            break;
        case 2:
            nextRow = beam.row + 1;
            nextCol = beam.col;
            break;
        case 3:
            nextRow = beam.row;
            nextCol = beam.col - 1;
            break;
        case 4:
            nextRow = beam.row - 1;
            nextCol = beam.col;
            break;
        default:
            break;
        }

        // exit if beam leaves grid
        if (nextRow < 0 || nextCol < 0 || nextRow >= floor.size() || nextCol >= floor[0].length()) {
            return;
        }

        // exit infinite loop of beeam
        if (lava[nextRow][nextCol] == '#' && prevTile == '#') {
            lavaCount++;
            if (lavaCount > loopLentgh) {
                return;
            }
        }

        if (floor[nextRow][nextCol] == '\\') {
            switch (beam.dir) {
            case 1:
                beam.dir = 2;
                break;
            case 2:
                beam.dir = 1;
                break;
            case 3:
                beam.dir = 4;
                break;
            case 4:
                beam.dir = 3;
                break;
            default:
                break;
            }
        } else if (floor[nextRow][nextCol] == '/') {
            switch (beam.dir) {
            case 1:
                beam.dir = 4;
                break;
            case 2:
                beam.dir = 3;
                break;
            case 3:
                beam.dir = 2;
                break;
            case 4:
                beam.dir = 1;
                break;
            default:
                break;
            }
        } else if (floor[nextRow][nextCol] == '-') {
            switch (beam.dir) {
            case 2:
            case 4:
                if (lava[nextRow][nextCol] != '#') {               // dont recurse alredy recursed beams
                    traceBeam(floor, lava, {nextRow, nextCol, 1}); // new beam rigthward
                    traceBeam(floor, lava, {nextRow, nextCol, 3}); // new beam leftward
                    return;
                }
                return;
            default:
                break;
            }
        } else if (floor[nextRow][nextCol] == '|') {
            switch (beam.dir) {
            case 1:
            case 3:
                if (lava[nextRow][nextCol] != '#') {               // dont recurse alredy recursed beams
                    traceBeam(floor, lava, {nextRow, nextCol, 2}); // new beam downward
                    traceBeam(floor, lava, {nextRow, nextCol, 4}); // new beam upward
                    return;
                }
                return;
            default:
                break;
            }
        }

        beam.row = nextRow;
        beam.col = nextCol;
        prevTile = lava[nextRow][nextCol];
        lava[nextRow][nextCol] = '#';
    }
}