#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int part1(std::vector<std::vector<int>> &readings);
int part2(std::vector<std::vector<int>> &readings);

int main() {
    std::fstream file("input.txt");

    std::vector<std::vector<int>> readings;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;
    while (std::getline(file, line)) {

        std::istringstream iss(line);

        std::vector<int> data;
        std::string word;
        while (iss >> word) {
            data.push_back(std::stoi(word));
        }

        readings.push_back(data);
    }

    std::cout << "Part1: " << part1(readings) << "\n";
    std::cout << "Part2: " << part2(readings) << "\n";

    return 0;
}

int part1(std::vector<std::vector<int>> &readings) {
    int sum = 0;
    for (auto data : readings) {
        std::vector<std::vector<int>> differences;
        std::vector<int> diffRow = data;
        differences.push_back(diffRow);

        bool notEnd = true;
        while (notEnd) {

            std::vector<int> tempRow;
            for (int i = 1; i < diffRow.size(); i++) {
                tempRow.push_back(diffRow[i] - diffRow[i - 1]);
            }
            differences.push_back(tempRow);
            diffRow = tempRow;

            for (auto val : diffRow) {
                if (val == 0) {
                    notEnd = false;
                    continue;
                }
                notEnd = true;
                break;
            }
        }

        for (int i = differences.size() - 1; i > 0; i--) {
            int extrapolate = differences[i - 1][differences[i - 1].size() - 1] + differences[i][differences[i].size() - 1];
            differences[i - 1].push_back(extrapolate);
        }
        sum += differences[0][differences[0].size() - 1];
    }

    return sum;
}

int part2(std::vector<std::vector<int>> &readings) {
    int sum = 0;
    for (auto data : readings) {
        std::vector<std::vector<int>> differences;
        std::vector<int> diffRow = data;
        differences.push_back(diffRow);

        bool notEnd = true;
        while (notEnd) {

            std::vector<int> tempRow;
            for (int i = 1; i < diffRow.size(); i++) {
                tempRow.push_back(diffRow[i] - diffRow[i - 1]);
            }
            differences.push_back(tempRow);
            diffRow = tempRow;

            for (auto val : diffRow) {
                if (val == 0) {
                    notEnd = false;
                    continue;
                }
                notEnd = true;
                break;
            }
        }

        for (int i = differences.size() - 1; i > 0; i--) {
            int extrapolate = differences[i - 1][0] - differences[i][0];
            differences[i - 1].insert(differences[i - 1].begin(), extrapolate);
        }
        sum += differences[0][0];
    }

    return sum;
}