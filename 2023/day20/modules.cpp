#include "modules.h"

namespace modules {

std::queue<Signal> FlipFlop::SendSignal(Signal signal) {
    std::queue<Signal> signalsSent;

    if (signal.value) {
        return signalsSent;
    }

    state = !state;
    for (auto destination : destinations) {
        signalsSent.push({this->state,
                          this,
                          destination});
        if (state) {
            this->highPulsesSent++;
        } else {
            this->lowPulsesSent++;
        }
    }
    return signalsSent;
}

void FlipFlop::ResetModule() {
    this->state = false;
}
void Conjunction::ResetModule() {
    for (auto &input : this->inputs) {
        input.second = false;
    }
}

void Conjunction::AddNewInput(std::string newInput) {
    if (newInput != "broadcaster") {
        inputs[newInput] = 0;
    }
}

std::queue<Signal> Conjunction::SendSignal(Signal signal) {
    std::queue<Signal> signalsSent;

    inputs[signal.origin->name] = signal.value;

    bool allTrue = std::all_of(inputs.begin(), inputs.end(),
                               [](const auto &entry) { return entry.second; });

    bool stateToSend = true;
    if (allTrue) {
        stateToSend = false;
    }
    for (auto destination : destinations) {

        signalsSent.push({stateToSend, this, destination});
        if (stateToSend) {
            this->highPulsesSent++;
        } else {
            this->lowPulsesSent++;
        }
    }

    return signalsSent;
}

std::queue<Signal> Broadcaster::SendSignal(Signal signal) {
    std::queue<Signal> signalsSent;
    for (auto destination : destinations) {
        signalsSent.push({signal.value, this, destination});
        if (signal.value) {
            this->highPulsesSent++;
        } else {
            this->lowPulsesSent++;
        }
    }
    return signalsSent;
}

} // namespace modules