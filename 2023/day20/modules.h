#ifndef MODULES_H
#define MODULES_H

#include <algorithm>
#include <cassert>
#include <iostream>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

namespace modules {

enum ModuleType {
    flipFlop,
    conjunction,
    broadcaster,
};

class Module;

struct Signal {
    bool value;
    Module *origin;
    Module *destination;
};

class Module {
public:
    std::string name;
    bool state = 0;
    std::vector<Module *> destinations;
    int highPulsesSent = 0;
    int lowPulsesSent = 0;
    ModuleType type;

    Module() {}
    Module(const std::string &name) : name(name) {}

    virtual std::queue<Signal> SendSignal(Signal signal) = 0;
    virtual void AddNewInput(std::string) = 0;
    virtual void ResetModule() = 0;
};

class FlipFlop : public Module {
public:
    FlipFlop() {}
    FlipFlop(const std::string &name) : Module(name) {}
    std::queue<Signal> SendSignal(Signal signal = {false, nullptr}) override;
    void AddNewInput(std::string) override{};
    void ResetModule() override;
};

class Conjunction : public Module {
public:
    std::unordered_map<std::string, bool> inputs;

    Conjunction() {}
    Conjunction(const std::string &name) : Module(name) {}

    void AddNewInput(std::string newInput) override;
    std::queue<Signal> SendSignal(Signal signal = {false, nullptr}) override;
    void ResetModule() override;
};

class Broadcaster : public Module {
public:
    Broadcaster() {}
    Broadcaster(const std::string &name) : Module(name) {}
    std::queue<Signal> SendSignal(Signal signal = {false, nullptr}) override;
    void AddNewInput(std::string) override{};
    void ResetModule() override{};
};

class OutputModule : public Module {
public:
    OutputModule() {}
    OutputModule(const std::string &name) : Module(name) {}
    std::queue<Signal> SendSignal(Signal signal = {false, nullptr}) override { return std::queue<Signal>(); };
    void AddNewInput(std::string) override{};
    void ResetModule() override{};
};

} // namespace modules
#endif