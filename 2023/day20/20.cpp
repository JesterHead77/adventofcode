#include "modules.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <queue>
#include <regex>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

std::vector<modules::Module *> cfg;
std::queue<modules::Signal> pulses;
unsigned long part1();
unsigned long part2();

int main() {
    // modules::basicTest();

    std::fstream file("input.txt");
    std::vector<std::vector<std::string>> fileVector;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    while (std::getline(file, line)) {

        std::istringstream iss(line);

        std::string word;
        std::vector<std::string> words;

        while (std::getline(iss, word, ' ')) {
            if (word.empty()) {
                continue;
            }
            if (word.back() == ',') {
                word.pop_back();
            }
            words.push_back(word);
        }

        modules::Module *newModule = nullptr;

        if (words[0][0] == '%') {
            newModule = new modules::FlipFlop(words[0].substr(1));
            newModule->type = modules::ModuleType::flipFlop;
            words[0] = words[0].substr(1); // remove module identifier
        } else if (words[0][0] == '&') {
            newModule = new modules::Conjunction(words[0].substr(1));
            newModule->type = modules::ModuleType::conjunction;
            words[0] = words[0].substr(1);
        } else if (words[0] == "broadcaster") {
            newModule = new modules::Broadcaster(words[0]);
            newModule->type = modules::ModuleType::broadcaster;
        } else {
            newModule = new modules::OutputModule(words[0]);
        }

        cfg.push_back(newModule);
        fileVector.push_back(words);
    }

    for (int i = 0; i < fileVector.size(); i++) {
        for (int j = 2; j < fileVector[i].size(); j++) {

            std::string nameToFind = fileVector[i][j];
            auto it = std::find_if(cfg.begin(), cfg.end(),
                                   [nameToFind](const auto &obj) {
                                       return obj->name == nameToFind;
                                   });

            if (it == cfg.end()) { // if counld not find module, it means that module has no configuration
                auto newModule = new modules::OutputModule(nameToFind);
                cfg.push_back(newModule);
                cfg[i]->destinations.push_back(newModule);
                continue;
            }
            cfg[i]->destinations.push_back(*it);

            if ((*it)->type == modules::ModuleType::conjunction) {
                (*it)->AddNewInput(fileVector[i][0]);
            }
        }
    }

    // std::cout << "Part1: " << part1() << "\n";
    std::cout << "Part2: " << part2() << "\n";

    return 0;
}

unsigned long part1() {
    int buttonPresses = 1000;
    std::queue<modules::Signal> tempQ;

    for (int i = 0; i < buttonPresses; i++) {

        std::string nameToFind = "broadcaster";
        auto it = std::find_if(cfg.begin(), cfg.end(),
                               [nameToFind](const auto &obj) {
                                   return obj->name == nameToFind;
                               });

        tempQ = (*it)->SendSignal({false, nullptr, (*it)}); // entry point

        while (!pulses.empty() || !tempQ.empty()) {

            while (!tempQ.empty()) {
                pulses.push(tempQ.front());
                tempQ.pop();
            }

            auto dest = pulses.front().destination;
            tempQ = dest->SendSignal(pulses.front());

            pulses.pop();
        }
    }

    unsigned long highPulsesSent = 0;
    unsigned long lowPulsesSent = buttonPresses;

    for (auto c : cfg) {
        highPulsesSent += c->highPulsesSent;
        lowPulsesSent += c->lowPulsesSent;
    }

    return highPulsesSent * lowPulsesSent;
}

void resetModules() {
    for (auto module : cfg) {
        module->ResetModule();
    }
}

unsigned long part2() {
    resetModules();

    std::vector<std::string> andGates = {"rv", "dc", "cq", "vp"};
    std::vector<int> cyclesLength;
    std::queue<modules::Signal> tempQ;

    for (auto &gate : andGates) {
        resetModules();

        auto nameToFindd = gate;
        auto itt = std::find_if(cfg.begin(), cfg.end(),
                                [nameToFindd](const auto &obj) {
                                    return obj->name == nameToFindd;
                                });
        auto gg = (*itt);

        int cycleLen = 0;
        bool finished = false;
        while (!finished) {
            cycleLen++;

            std::string nameToFind = "broadcaster";
            auto it = std::find_if(cfg.begin(), cfg.end(),
                                   [nameToFind](const auto &obj) {
                                       return obj->name == nameToFind;
                                   });

            tempQ = (*it)->SendSignal({false, nullptr, (*it)}); // entry point

            while (!pulses.empty() || !tempQ.empty()) {

                while (!tempQ.empty()) {
                    pulses.push(tempQ.front());
                    tempQ.pop();
                }

                auto dest = pulses.front().destination;

                tempQ = dest->SendSignal(pulses.front());

                if (dest->name == gate) {
                    if (tempQ.front().value == true) {
                        cyclesLength.push_back(cycleLen);
                        finished = true;
                        std::cout << cycleLen << std::endl;
                        break;
                    }
                }

                pulses.pop();
            }
         }
    }

    return std::accumulate(cyclesLength.begin(), cyclesLength.end(), 1UL, std::lcm<unsigned long, unsigned long>);
}

