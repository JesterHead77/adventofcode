#include "split.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

int part1(std::vector<std::pair<std::string, int>> &poker);
int part2(std::vector<std::pair<std::string, int>> &poker);
bool compare(std::string hand1, std::string hand2);
int hand_strength(std::string hand);
bool compare2(std::string hand1, std::string hand2);
int hand_strength2(std::string hand);

std::unordered_map<char, int> cards = {
    {'2', 2},
    {'3', 3},
    {'4', 4},
    {'5', 5},
    {'6', 6},
    {'7', 7},
    {'8', 8},
    {'9', 9},
    {'T', 10},
    {'J', 11},
    {'Q', 12},
    {'K', 13},
    {'A', 14},
};

std::unordered_map<char, int> cards2 = {
    {'2', 2},
    {'3', 3},
    {'4', 4},
    {'5', 5},
    {'6', 6},
    {'7', 7},
    {'8', 8},
    {'9', 9},
    {'T', 10},
    {'J', 1},
    {'Q', 12},
    {'K', 13},
    {'A', 14},
};

int main() {
    std::fstream file("input.txt");

    std::vector<std::pair<std::string, int>> poker;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }
    std::string line;
    int k = 0;

    while (std::getline(file, line)) {
        std::vector<std::string> words = my::split(line, ' ');
        poker.push_back({words[0], std::stoi(words[1])});
    }

    std::cout << "Part1: " << part1(poker) << "\n";
    std::cout << "Part2: " << part2(poker) << "\n";

    return 0;
}

int part1(std::vector<std::pair<std::string, int>> &poker) {

    auto sorter = [](const std::pair<std::string, int> &hand1, const std::pair<std::string, int> &hand2) {
        return compare(hand2.first, hand1.first);
    };

    std::sort(poker.begin(), poker.end(), sorter);

    int sum = 0;

    for (int i = 0; i < poker.size(); i++) {
        sum += poker[i].second * (i + 1);
    }

    return sum;
}

int part2(std::vector<std::pair<std::string, int>> &poker) {
    auto sorter1 = [](const std::pair<std::string, int> &hand1, const std::pair<std::string, int> &hand2) {
        return compare2(hand2.first, hand1.first);
    };

    std::sort(poker.begin(), poker.end(), sorter1);

    int sum = 0;

    for (int i = 0; i < poker.size(); i++) {
        sum += poker[i].second * (i + 1);
    }

    return sum;
}

bool compare(std::string hand1, std::string hand2) {
    int h1 = hand_strength(hand1);
    int h2 = hand_strength(hand2);
    if (h1 > h2) {
        return true;
    }
    if (h2 > h1) {
        return false;
    }
    for (int i = 0; i < hand1.length(); i++) {
        if (cards[hand1[i]] > cards[hand2[i]]) {
            return true;
        }
        if (cards[hand2[i]] > cards[hand1[i]]) {
            return false;
        }
    }
    std::cout << hand1;
    return false; // same
}

int hand_strength(std::string hand) {
    std::unordered_map<char, int> h;

    for (char ch : hand) {
        if (ch != 0) {
            h[ch]++;
        }
    }

    auto it = h.begin(); // iterate over the map

    if (it->second == 5) {
        return 6; // five of a kind
    }
    if (it->second == 4 || (++it)->second == 4) {
        return 5; // four of a kind
    }

    it = h.begin();

    if ((it->second == 3 && (++it)->second == 2) ||
        (it->second == 2 && (++it)->second == 3)) {
        return 4; // full house
    }

    it = h.begin();

    if (it->second == 3 ||
        (++it)->second == 3 ||
        (++it)->second == 3) {
        return 3; // three of a kind
    }
    if (h.size() == 3) {
        return 2; // two pair
    }
    if (h.size() == 4) {
        return 1; // one pair
    }
    return 0; // high card
}

bool compare2(std::string hand1, std::string hand2) {
    int h1 = hand_strength2(hand1);
    int h2 = hand_strength2(hand2);
    if (h1 > h2) {
        return true;
    }
    if (h2 > h1) {
        return false;
    }
    for (int i = 0; i < hand1.length(); i++) {
        if (cards2[hand1[i]] > cards2[hand2[i]]) {
            return true;
        }
        if (cards2[hand2[i]] > cards2[hand1[i]]) {
            return false;
        }
    }
    std::cout << hand1;
    return false; // same
}

int hand_strength2(std::string hand) {
    std::unordered_map<char, int> h;

    for (char ch : hand) {
        if (ch != 0) {
            h[ch]++;
        }
    }

    int jokers = 0;
    if (h.count('J') > 0) {
        jokers = h['J'];
    }

    auto it = h.begin(); // iterate over the map

    if (it->second == 5) {
        return 6; // five of a kind
    }
    if (it->second == 4 || (++it)->second == 4) {
        if (jokers > 0) {
            return 6; // upgraded to five of a kind
        }
        return 5; // four of a kind
    }

    it = h.begin();

    if ((it->second == 3 && (++it)->second == 2) ||
        (it->second == 2 && (++it)->second == 3)) {
        if (jokers > 0) {
            return 6; // upgraded to five of a kind
        }
        return 4; // full house
    }

    it = h.begin();

    if (it->second == 3 ||
        (++it)->second == 3 ||
        (++it)->second == 3) {
        if (jokers > 0) {
            return 5; // upgraded to four of a kind
        }
        return 3; // three of a kind
    }
    if (h.size() == 3) {
        if (jokers == 1) {
            return 4; // upgraded to full house
        } else if (jokers == 2) {
            return 5; // upgraded to four of a kind
        }
        return 2; // two pair
    }
    if (h.size() == 4) {
        if (jokers == 1 || jokers == 2) {
            return 3; // upgraded to three of a kind
        }
        return 1; // one pair
    }

    if (jokers == 1) {
        return 1; // upgraded to one pair
    }
    return 0; // high card
}
