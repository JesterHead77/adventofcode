#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

struct Instruction {
    char dir;
    int length;
    std::string color;
};

struct Point {
    long row;
    long col;
};

long calcGrid(std::vector<Point> &points);

int main() {
    std::fstream file("input.txt");

    std::vector<Instruction> digPlan;
    std::vector<Point> points;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    long row = 0;
    long col = 0;

    std::string line;
    while (std::getline(file, line)) {

        std::istringstream iss(line);

        std::string word;
        std::vector<std::string> words;

        while (std::getline(iss, word, ' ')) {
            words.push_back(word);
        }

        Instruction in = {words[0][0],
                          std::stoi(words[1]),
                          words[2]};

        if (in.dir == 'R') {
            col += in.length;
        } else if (in.dir == 'U') {
            row -= in.length;
        } else if (in.dir == 'L') {
            col -= in.length;
        } else if (in.dir == 'D') {
            row += in.length;
        }

        points.push_back({row, col});
        digPlan.push_back(in);
    }

    std::cout << "Part1: " << calcGrid(points) << "\n";

    ////////////////////////////////////////////////////////////////////////////
    std::vector<Point> points2;
    row = 0;
    col = 0;
    for (auto instruction : digPlan) {
        instruction.length = std::stoi(
            instruction.color.substr(2, 5), nullptr, 16);
        switch (instruction.color[7]) {
        case '0':
            instruction.dir = 'R';
            col += instruction.length;
            break;
        case '1':
            instruction.dir = 'D';
            row += instruction.length;
            break;
        case '2':
            instruction.dir = 'L';
            col -= instruction.length;
            break;
        case '3':
            instruction.dir = 'U';
            row -= instruction.length;
            break;

        default:
            break;
        }
        points2.push_back({row, col});
    }

    std::cout << "Part2: " << calcGrid(points2) << "\n";

    return 0;
}

long calcGrid(std::vector<Point> &points) {

    long boundary = 0;
    
    // Shoelace Formula
    long area = 0;
    for (int i = points.size() - 1; i >= 0; i--) {
        if (i == 0) {
            area += (points[i].row * points[points.size() - 1].col);
            area -= (points[i].col * points[points.size() - 1].row);
            boundary += abs(points[i].row - points[points.size() - 1].row) + abs(points[i].col - points[points.size() - 1].col); // not shoelace, works only in this case
            break;
        }
        area += (points[i].row * points[i - 1].col);
        area -= (points[i].col * points[i - 1].row);
        boundary += abs(points[i].row - points[i - 1].row) + abs(points[i].col - points[i - 1].col); // not shoelace, works only in this case
    }
    area /= 2;

    // Pick theorem
    long inside = area - boundary / 2 + 1;

    return inside + boundary;
}
