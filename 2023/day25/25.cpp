#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

int part1();
int part2();

std::vector<std::pair<int, int>> connections;
std::map<std::string, int> nodeMap;

int GetNodeIndex(std::string &node) {
    if (nodeMap.find(node) != nodeMap.cend())
        return nodeMap[node];
    int index = nodeMap.size();
    nodeMap[node] = index;
    return index;
};

int main() {
    std::fstream file("input.txt");

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    while (std::getline(file, line)) {
        std::stringstream ss(line);
        std::string name;
        ss >> name;
        name = name.substr(0, name.size() - 1);
        int baseIndex = GetNodeIndex(name);
        while (ss >> name)
            connections.emplace_back(baseIndex, GetNodeIndex(name));
    }

    std::cout << "Part1: " << part1() << "\n";
    // std::cout << "Part2: " << part2() << "\n";

    return 0;
}

//from wikipedia Stoer–Wagner algorithm
std::pair<int, std::vector<int>> globalMinCut(std::vector<std::vector<int>> mat) {
    std::pair<int, std::vector<int>> best = {0x7fffffff, {}};
    int n = mat.size();
    std::vector<std::vector<int>> co(n);

    for (int i = 0; i < n; i++)
        co[i] = {i};

    for (int ph = 1; ph < n; ph++) {
        std::vector<int> w = mat[0];
        size_t s = 0, t = 0;
        for (int it = 0; it < n - ph; it++) { // O(V^2) -> O(E log V) with prio. queue
            w[t] = -0x7fffffff - 1;
            s = t, t = max_element(w.begin(), w.end()) - w.begin();
            for (int i = 0; i < n; i++)
                w[i] += mat[t][i];
        }
        best = min(best, {w[t] - mat[t][t], co[t]});
        co[s].insert(co[s].end(), co[t].begin(), co[t].end());
        for (int i = 0; i < n; i++)
            mat[s][i] += mat[t][i];
        for (int i = 0; i < n; i++)
            mat[i][s] = mat[s][i];
        mat[0][t] = -0x7fffffff - 1;
    }

    return best;
}

int part1() {
    std::vector<std::vector<int>> adj(nodeMap.size(), std::vector<int>(nodeMap.size(), 0));
	for (const auto& p : connections)
		adj[p.first][p.second] = adj[p.second][p.first] = 1;

	auto result = globalMinCut(adj);
    return result.second.size() * (nodeMap.size() - result.second.size());
}