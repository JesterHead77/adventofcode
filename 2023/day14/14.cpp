#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

int part1(std::vector<std::string> &platform);
int part2(std::vector<std::string> &platform);
int calculateLoad(std::vector<std::string> &platform);
void tiltPlatformNorth(std::vector<std::string> &platform);
void tiltPlatformSouth(std::vector<std::string> &platform);
void tiltPlatformWest(std::vector<std::string> &platform);
void tiltPlatformEast(std::vector<std::string> &platform);

int main() {
    std::fstream file("input.txt");

    std::vector<std::string> platform;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    while (std::getline(file, line)) {
        platform.push_back(line);
    }

    // std::cout << "Part1: " << part1(platform) << "\n";
    std::cout << "Part2: " << part2(platform) << "\n";

    return 0;
}

int part1(std::vector<std::string> &platform) {

    tiltPlatformNorth(platform);
    for (auto line : platform) {
        std::cout << line << std::endl;
    }

    return calculateLoad(platform);
}

int part2(std::vector<std::string> &platform) {
    int cycles = 1;
    int total = 1000000000;
    int cycle_len = 0;
    int index = 0;

    std::vector<std::vector<std::string>> cache;

    while (true) {

        tiltPlatformNorth(platform);
        tiltPlatformWest(platform);
        tiltPlatformSouth(platform);
        tiltPlatformEast(platform);

        auto it = std::find(cache.begin(), cache.end(), platform);

        if (it != cache.end()) {
            index = std::distance(cache.begin(), it);

            cycle_len = cycles - index - 1;

            break;
        }

        cycles++;
        cache.push_back(platform);
    }

    for (int i = 0; i < (total - cycles) % cycle_len; i++) {
        tiltPlatformNorth(platform);
        tiltPlatformWest(platform);
        tiltPlatformSouth(platform);
        tiltPlatformEast(platform);
    }

    return calculateLoad(platform);
}

void tiltPlatformNorth(std::vector<std::string> &platform) {
    for (int i = 1; i < platform.size(); i++) {
        for (int j = 0; j < platform[0].length(); j++) {
            if (platform[i][j] == 'O') {
                for (int k = 1; k <= i; k++) {
                    if (platform[i - k][j] == '.') {
                        platform[i - k][j] = 'O';
                        platform[i - k + 1][j] = '.';
                        continue;
                    }
                    break;
                }
            }
        }
    }
}
void tiltPlatformSouth(std::vector<std::string> &platform) {
    for (int i = platform.size() - 2; i >= 0; i--) {
        for (int j = 0; j < platform[0].length(); j++) {
            if (platform[i][j] == 'O') {
                for (int k = 1; k < platform.size() - i; k++) {
                    if (platform[i + k][j] == '.') {
                        platform[i + k][j] = 'O';
                        platform[i + k - 1][j] = '.';
                        continue;
                    }
                    break;
                }
            }
        }
    }
}
void tiltPlatformWest(std::vector<std::string> &platform) {
    for (int i = 1; i < platform[0].length(); i++) {
        for (int j = 0; j < platform.size(); j++) {
            if (platform[j][i] == 'O') {
                for (int k = 1; k <= i; k++) {
                    if (platform[j][i - k] == '.') {
                        platform[j][i - k] = 'O';
                        platform[j][i - k + 1] = '.';
                        continue;
                    }
                    break;
                }
            }
        }
    }
}
void tiltPlatformEast(std::vector<std::string> &platform) {
    for (int i = platform[0].length() - 2; i >= 0; i--) {
        for (int j = 0; j < platform.size(); j++) {
            if (platform[j][i] == 'O') {
                for (int k = 1; k < platform[0].length(); k++) {
                    if (platform[j][i + k] == '.') {
                        platform[j][i + k] = 'O';
                        platform[j][i + k - 1] = '.';
                        continue;
                    }
                    break;
                }
            }
        }
    }
}

int calculateLoad(std::vector<std::string> &platform) {
    int load = 0;

    for (int i = 0; i < platform.size(); i++) {
        for (int j = 0; j < platform[0].length(); j++) {
            if (platform[i][j] == 'O') {
                load += platform.size() - i;
            }
        }
    }

    return load;
}