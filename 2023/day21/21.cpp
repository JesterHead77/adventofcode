#include <cmath>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <tuple>
#include <vector>

struct Step {
    int row;
    int col;
};

struct StepComparator {
    bool operator()(const Step &lhs, const Step &rhs) const {
        return std::tie(lhs.row, lhs.col) < std::tie(rhs.row, rhs.col);
    }
};

unsigned long part1(std::vector<std::string> &garden, Step &startingStep);
unsigned long part2(std::vector<std::string> &garden, Step &startingStep);

int main() {
    std::fstream file("input.txt");
    std::vector<std::string> garden;
    Step startingStep;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    int row = 0;
    while (std::getline(file, line)) {

        bool startFound = false;
        if (!startFound) {
            for (int i = 0; i < line.length(); i++) {
                if (line[i] == 'S') {
                    startingStep.row = row;
                    startingStep.col = i;
                    startFound = true;
                }
            }
        }

        garden.push_back(line);
        row++;
    }

    std::cout << "Part1: " << part1(garden, startingStep) << "\n";
    std::cout << "Part2: " << part2(garden, startingStep) << "\n";

    return 0;
}

unsigned long part1(std::vector<std::string> &garden, Step &startingStep) {
    int stepsNumber = 64;
    std::set<Step, StepComparator> steps;
    steps.insert(startingStep);

    for (int i = 0; i < stepsNumber; i++) {
        std::set<Step, StepComparator> newSteps;

        for (auto step : steps) {
            if (step.row > 0 && garden[step.row - 1][step.col] != '#') {
                newSteps.insert({step.row - 1, step.col});
            }
            if (step.row < garden.size() - 1 && garden[step.row + 1][step.col] != '#') {
                newSteps.insert({step.row + 1, step.col});
            }
            if (step.col > 0 && garden[step.row][step.col - 1] != '#') {
                newSteps.insert({step.row, step.col - 1});
            }
            if (step.col < garden[0].length() - 1 && garden[step.row][step.col + 1] != '#') {
                newSteps.insert({step.row, step.col + 1});
            }
        }

        steps.clear();
        steps.insert(newSteps.begin(), newSteps.end());
    }

    return steps.size();
}

unsigned long part2(std::vector<std::string> &garden, Step &startingStep) {

    std::vector<std::string> bigGarden;
    for (int i = 0; i < 7; i++) {
        for (auto line : garden) {
            std::string aa;
            for (int j = 0; j < 7; j++) {
                aa += line;
            }
            bigGarden.push_back(aa);
        }
    }

    std::vector<int> pc;

    int stepsNumber = 328;
    std::set<Step, StepComparator> steps;
    steps.insert({static_cast<int>(bigGarden.size() / 2), static_cast<int>(bigGarden.size() / 2)});

    for (int i = 1; i < stepsNumber; i++) {
        std::set<Step, StepComparator> newSteps;

        for (auto step : steps) {
            if (step.row > 0 && bigGarden[step.row - 1][step.col] != '#') {
                newSteps.insert({step.row - 1, step.col});
            }
            if (step.row < bigGarden.size() - 1 && bigGarden[step.row + 1][step.col] != '#') {
                newSteps.insert({step.row + 1, step.col});
            }
            if (step.col > 0 && bigGarden[step.row][step.col - 1] != '#') {
                newSteps.insert({step.row, step.col - 1});
            }
            if (step.col < bigGarden[0].length() - 1 && bigGarden[step.row][step.col + 1] != '#') {
                newSteps.insert({step.row, step.col + 1});
            }
        }

        steps.clear();
        steps.insert(newSteps.begin(), newSteps.end());
        if (i == 65 || i == 196 || i == 327) {
            pc.push_back(steps.size());
        }
    }

    unsigned long a, b, c;

    // Calculate coefficients a, b, and c of the quadratic function
    a = (1 * (pc[0] - pc[2]) + 2 * (pc[1] - pc[0])) /
        ((0 - 1) * (0 - 2) * (1 - 2));
    b = (pc[1] - pc[0]) / (1) - a * (1);
    c = pc[0];

    unsigned long val = 202300;
    return a * pow(val, 2) + b * val + c;
}
