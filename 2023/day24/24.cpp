#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

struct Hail {
    double x, y, z;
    double vx, vy, vz;
};

int part1(std::vector<Hail> &hailstones);
int part2();

bool intersectionInBounds(Hail &hail1, Hail &hail2) {
    double x1 = hail1.x;
    double y1 = hail1.y;
    double x2 = hail1.x + hail1.vx;
    double y2 = hail1.y + hail1.vy;
    double x3 = hail2.x;
    double y3 = hail2.y;
    double x4 = hail2.x + hail2.vx;
    double y4 = hail2.y + hail2.vy;

    double m1 = (y2 - y1) / (x2 - x1);
    double m2 = (y4 - y3) / (x4 - x3);

    if (m1 == m2) {
        return false;
    }

    double x = (m1 * x1 - m2 * x3 + y3 - y1) / (m1 - m2);
    double y = m1 * (x - x1) + y1;

    if ((x2-x1 < 0 && x > x1) || (x2-x1 > 0 && x < x1) || (x4-x3 < 0 && x > x3) || (x4-x3 > 0 && x < x3)){
        return false;
    }

    if (x >= 200000000000000 && x <= 400000000000000 && y >= 200000000000000 && y <= 400000000000000) {
    // if (x >= 7 && x <= 27 && y >= 7 && y <= 27) {
        return true;
    }

    return false;
}

int main() {
    std::fstream file("input.txt");
    std::vector<Hail> hailstones;

    if (!file.is_open()) {
        std::cerr << "Failed to open "
                  << "\n";
        return 1;
    }

    std::string line;
    std::regex pattern("-?[0-9]+");
    while (std::getline(file, line)) {

        std::smatch match;
        std::vector<double> matches;
        while (std::regex_search(line, match, pattern)) {
            matches.push_back(std::stod(match[0].str()));
            line = match.suffix();
        }

        Hail newHail = {matches[0], matches[1], matches[2], matches[3], matches[4], matches[5]};
        hailstones.push_back(newHail);
    }

    std::cout << "Part1: " << part1(hailstones) << "\n";
    // std::cout << "Part2: " << part2() << "\n";

    return 0;
}

int part1(std::vector<Hail> &hailstones) {
    int intersections = 0;
    for (int i = 0; i < hailstones.size(); i++) {
        for (int j = i; j < hailstones.size(); j++) {
            if (i == j) {
                continue;
            }
            if (intersectionInBounds(hailstones[i], hailstones[j])) {
                intersections++;
            }
        }
    }
    return intersections;
}